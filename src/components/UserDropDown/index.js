import {connect} from 'react-redux';
import {postLogin, getUser, logoutUser} from './../../modules/Login/actions';
import UserIndex from './components/index';

const mapStateToProps = (state, props) => {
    const {loading, userData} = state.user
    return {
        loading,
        userData
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        postLogin: (props)=>{
            dispatch(postLogin(props))
        },
        getUser: (props)=>{
            dispatch(getUser(props))
        },
        logoutUser: (props)=>{
            dispatch(logoutUser(props))
        },
    };
};

let Index = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserIndex);

export default Index;

