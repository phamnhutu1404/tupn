import React from 'react'
import {Row, Button, Col, Dropdown, Menu} from 'antd'
import { Form, Input, Checkbox } from 'antd'
import {DownOutlined, UserOutlined} from '@ant-design/icons'
import {router, url} from "./../../../system/routing";

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectionType: []
        };
    }

    logOut = ()=>{
        const {userData = {}} = this.props
        this.props.logoutUser({username: userData['username'], role: userData['role']})
        router.redirect(url.to('bank.login'))
    }

    render() {
        const {userData = {}} = this.props

        const menu = (
            <Menu>
                <Menu.Item onClick={this.logOut}>
                    Logout
                </Menu.Item>
            </Menu>
        );
        return (
            <Dropdown overlay={menu}>
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                    <b>Welcome {userData['username'] || null}</b> <DownOutlined />
                </a>
            </Dropdown>
        );
    }
}

export default Login;
