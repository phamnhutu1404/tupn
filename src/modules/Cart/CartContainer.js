import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getListCart, updateProductToCart, removeProductToCart} from './actions';
import CartContainer from './components/index';

const mapStateToProps = (state, props) => {
    const {loading, cartList} = state.cart
    return {
        loading,
        cartList
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getListCart: ()=>{
            dispatch(getListCart())
        },
        updateProductToCart: (filter)=>{
            dispatch(updateProductToCart(filter))
        },
        removeProductToCart: (filter)=>{
            dispatch(removeProductToCart(filter))
        }
    };
};

let Container = connect(
    mapStateToProps,
    mapDispatchToProps
)(CartContainer);

Container.defaultProps = {
    input: {},
    canCreate: false,
};

Container.propTypes = {
    input: PropTypes.object,
    canCreate: PropTypes.bool,
};

export default Container;

