import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const getListCart = createAction(ACTION.GET_CART.REQUEST, data => data);
export const createProductToCart = createAction(ACTION.ADD_PRODUCT_TO_CART.REQUEST, data => data);
export const updateProductToCart = createAction(ACTION.UPDATE_PRODUCT_TO_CART.REQUEST, data => data);
export const removeProductToCart = createAction(ACTION.REMOVE_PRODUCT_TO_CART.REQUEST, data => data);
export const clearState = createAction(ACTION.CLEAR_STATE);
