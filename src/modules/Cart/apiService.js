import {api} from '../../system/api';

export default {
    //getListCart: (params) => api.get(`packages/barcode`,{singleRequest: true}),
    getListCart: (params) => new Promise((resolve, reject) => {
        // let data = [
        //     {
        //         key: '1',
        //         name: 'Mike',
        //         age: 32,
        //         address: '10 Downing Street',
        //     },
        //     {
        //         key: '2',
        //         name: 'John',
        //         age: 42,
        //         address: '10 Downing Street',
        //     },
        // ];
        let data = [
            {id:1,  key: '1', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            {id:2,  key: '2', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            // {id:3,  key: '3', quantity: 1, price:1000, amount:'200.000' ,src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            // {id:4,  key: '4', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            // {id:5,  key: '5', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            // {id:6,  key: '6', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            // {id:7,  key: '7', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            // {id:8,  key: '8', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            // {id:9,  key: '9', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            // {id:10,  key: '10', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            // {id:11,  key: '11', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
        ]
        resolve({error: false, data});
    }),
    //updateCart: (params) => api.put(`packages/${params.packageCode}/update-weight`, {weight_net:params.weight_net}),
    updateCart: (params) => new Promise((resolve, reject) => {
        console.log(params,'params')
        let {id, quantity, cartList} = params;
        cartList = cartList.map( cartItem =>{
            if( cartItem.id === id ){
                cartItem = {...cartItem,quantity: quantity}
                return cartItem
            }
            return cartItem
        })
        console.log(cartList,'params params')
        resolve({error: false, data: cartList});
    }),
    getBarcodePackages: (barcode) => api.get(`packages/barcode/${barcode}`, {singleRequest: true}),
    createByBarcode: (params) => api.post('packages/barcode', {...params}, {singleRequest: true}),
    linkOrderPackage: (packageCode, orderCode) => api.post(`packages/${packageCode}/link-order`, {order_code: orderCode}),
    updateDimension: (packageCode, data) => api.put(`packages/${packageCode}/update-volume`, data),
    updateWeightPackage: (params) => api.put(`packages/${params.packageCode}/update-weight`, {weight_net:params.weight_net}),
    updateRelatedPackage: (id, data) => api.put(`packages/${id}/update`, data),
    getPackages: (params) => api.get('packages', {params}, {singleRequest: true}),
    getPackagePrint: (id) => api.get(`packages/${id}/print-barcode`, {singleRequest: true}),
    getPackage: (id) => api.get(`packages/${id}`, {singleRequest: true}),
};
