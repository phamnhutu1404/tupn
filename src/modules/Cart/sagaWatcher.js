import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.GET_CART.REQUEST, saga.getListCart)
    // yield takeLatest(ACTION.GET_CART.SUCCESS, saga.onCreatePackageSameBarcodeSuccess)
    // yield takeLatest(ACTION.GET_CART.FAILED, saga.onCreatePackageSameBarcodeFailed)

    yield takeLatest(ACTION.UPDATE_PRODUCT_TO_CART.REQUEST, saga.updateCart)
}
