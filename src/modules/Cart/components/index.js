import React from 'react'
import {Row, Table, Radio, Button, Empty, InputNumber} from 'antd'
import {url,router,Link} from './../../../system/routing/index'
import BlockItem from './Block'
import {MAX_QUANTITY, MIN_QUANTITY} from "./../constants"
import {MinusOutlined, PlusOutlined} from "@ant-design/icons";

const ButtonGroup = Button.Group

const data = [
    {id:1, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:2, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:3, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:4, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:5, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:6, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:7, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:8, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:9, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:10, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:11, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
    {id:12, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
]

const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: record => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
    }),
};


class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectionType: []
        };
    }

    componentDidMount() {
        this.props.getListCart();
    }

    //decline Quantity
    declineQuantity = (id, quantity, event)=>{
        const {cartList} = this.props
        if( MIN_QUANTITY < quantity){
            quantity--
            this.props.updateProductToCart({id, quantity, cartList})
            this.setState({quantity})
        }
    }
    // increase Quantity
    increaseQuantity = (id, quantity, event)=>{
        const {cartList} = this.props
        if( quantity<MAX_QUANTITY){
            quantity++
            this.props.updateProductToCart({id, quantity, cartList})
            this.setState({quantity})
        }
    }

    // onChange Quantity
    onChangeQuantity = (id, quantity)=>{
        const {cartList} = this.props
        if(Number.isInteger(quantity)){
            this.props.updateProductToCart({id, quantity, cartList})
            this.setState({quantity})
        }
    }

    render() {
        const {selectionType} = this.state
        const {cartList} = this.props
        const columns = [
            {
                title: 'San pham',
                dataIndex: 'info_product',
                render: (text, record) =>
                    <div className={'info-product'}>
                        <Link to={'home'}>
                            <img width={80} style={{float:'left'}} src={record.src} alt={record.title}/>&nbsp;
                            <span className={'info-product-title'}>{record.title}</span>
                        </Link>
                    </div>,
            },
            {
                title: 'Don gia',
                dataIndex: 'price',
                render: (text, record) =>
                        <div className={'product-price'}>
                            <div className="price2">
                                <span>650.000</span>
                                <span>₫</span>
                            </div>
                            <div className="price1">
                                <span>650.000</span>
                                <span>₫</span>
                            </div>
                        </div>
            },
            {
                title: 'So luong',
                dataIndex: 'quantity',
                render: (text, record) =>
                    <div className="ant-btn-group">
                        <ButtonGroup>
                            <Button onClick={this.declineQuantity.bind(this,record.id, record.quantity)}>
                                <MinusOutlined />
                            </Button>
                            <InputNumber className={'quantity-product'}
                                         min={MIN_QUANTITY} max={MAX_QUANTITY}
                                         value={record.quantity} defaultValue={record.quantity}
                                         onChange={this.onChangeQuantity.bind(this, record.id)} />
                            <Button onClick={this.increaseQuantity.bind(this,record.id, record.quantity)}>
                                <PlusOutlined />
                            </Button>
                        </ButtonGroup>
                    </div>,
            },
            {
                title: 'So tien',
                dataIndex: 'amount',
                render: (text, record) =>
                    <div className={'product-price'}>
                        <div className="price1">
                            <span>{record.amount}</span>
                            <span>₫</span>
                        </div>
                    </div>
            },
            {
                title: 'Thao tac',
                dataIndex: 'action',
                render: (text, record) =>
                    <div className={'info-product'}>
                        Xóa
                    </div>,
            },
        ];

        return (
            <div className={'cart-content'}>
                {cartList.length? <React.Fragment>
                        <Table
                            rowSelection={{
                                type: selectionType,
                                ...rowSelection,
                            }}
                            className={'cart-content__item-product'}
                            columns={columns}
                            dataSource={cartList}
                            pagination={false}
                        />
                        <div style={{textAlign:'right'}}>
                            <Link to={'cart.checkout'}><Button size='large' type="danger">Đặt hàng</Button></Link>
                        </div>
                </React.Fragment>
                    :
                    <React.Fragment>
                    <Empty>
                        <Link to={'home'}><Button size='large'>Mua ngay</Button></Link>
                    </Empty>
                    <BlockItem title={"Co the ban thich"} data={data}/>
                </React.Fragment>}
            </div>
        );
    }
}

export default Cart;
