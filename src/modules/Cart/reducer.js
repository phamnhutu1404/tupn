import {combineReducers} from 'redux';
import * as ACTION from './constants';

let loading = (state = false, action) => {
    switch (action.type) {
        case ACTION.GET_CART.REQUEST:
            return true;

        case ACTION.GET_CART.SUCCESS:
        case ACTION.GET_CART.FAILED:
        case ACTION.CLEAR_STATE:
            return false;

        default:
            return state;
    }
};

let cartList = (state = [], action) => {
    switch (action.type) {
        case ACTION.GET_CART.REQUEST:
            return [];
        case ACTION.GET_CART.FAILED:
        case ACTION.CLEAR_STATE:
            return [];
        case ACTION.GET_CART.SUCCESS:
            return action.payload
        case ACTION.UPDATE_PRODUCT_TO_CART.SUCCESS:
            return action.payload
        default:
            return state;
    }
};

export default combineReducers({
    loading,
    cartList,
});
