export const GET_CART = {
    REQUEST: 'CART.GET_CART.REQUEST',
    SUCCESS: 'CART.GET_CART.SUCCESS',
    FAILED: 'CART.GET_CART.FAILED',
};

export const ADD_PRODUCT_TO_CART = {
    REQUEST: 'CART.ADD_PRODUCT_TO_CART.REQUEST',
    SUCCESS: 'CART.ADD_PRODUCT_TO_CART.SUCCESS',
    FAILED: 'CART.ADD_PRODUCT_TO_CART.FAILED',
};


export const UPDATE_PRODUCT_TO_CART = {
    REQUEST: 'CART.UPDATE_PRODUCT_TO_CART.REQUEST',
    SUCCESS: 'CART.UPDATE_PRODUCT_TO_CART.SUCCESS',
    FAILED: 'CART.UPDATE_PRODUCT_TO_CART.FAILED',
};

export const REMOVE_PRODUCT_TO_CART = {
    REQUEST: 'CART.REMOVE_PRODUCT_TO_CART.REQUEST',
    SUCCESS: 'CART.REMOVE_PRODUCT_TO_CART.SUCCESS',
    FAILED: 'CART.REMOVE_PRODUCT_TO_CART.FAILED',
};

export const CLEAR_STATE = 'CART.GET_HOME.CLEAR_STATE';

//Number quantity product customer can order
export const MIN_QUANTITY = 1
export const MAX_QUANTITY = 99