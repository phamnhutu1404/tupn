import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.CREATE_TASK_TELLER.REQUEST, saga.createTaskTeller);
    yield takeLatest(ACTION.TASK_TELLER_SAVING.REQUEST, saga.savingTaskTeller);
    yield takeLatest(ACTION.TASK_TELLER_FINISH.REQUEST, saga.finishTaskTeller);
}