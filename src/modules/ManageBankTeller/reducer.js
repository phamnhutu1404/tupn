import {combineReducers} from 'redux';
import * as ACTION from './constants';

let loading = (state = false, action) => {
    switch (action.type) {
        case ACTION.CREATE_TASK_TELLER.REQUEST:
            return true;

        case ACTION.CREATE_TASK_TELLER.SUCCESS:
        case ACTION.CREATE_TASK_TELLER.FAILED:
        case ACTION.CLEAR_STATE:
            return false;
        default:
            return state;
    }
};

let createTaskTeller = (state = [], action) => {
    switch (action.type) {
        case ACTION.CREATE_TASK_TELLER.REQUEST:
            return [];
        case ACTION.CREATE_TASK_TELLER.FAILED:
        case ACTION.CLEAR_STATE:
            return [];
        case ACTION.CREATE_TASK_TELLER.SUCCESS:
            return action.payload
        default:
            return state;
    }
};

let savingTaskTeller = (state = {}, action) => {
    switch (action.type) {
        case ACTION.TASK_TELLER_SAVING.REQUEST:
            return {};
        case ACTION.TASK_TELLER_SAVING.FAILED:
            return {};
        case ACTION.TASK_TELLER_SAVING.SUCCESS:
            return action.payload
        default:
            return state;
    }
};

let finishTaskTeller = (state = [], action) => {
    switch (action.type) {
        case ACTION.TASK_TELLER_FINISH.REQUEST:
            return [];
        case ACTION.TASK_TELLER_FINISH.FAILED:
            return [];
        case ACTION.TASK_TELLER_FINISH.SUCCESS:
            return action.payload
        default:
            return state;
    }
};


let loadingSaving = (state = false, action) => {
    switch (action.type) {
        case ACTION.TASK_TELLER_SAVING.REQUEST:
            return true;
        case ACTION.TASK_TELLER_SAVING.SUCCESS:
        case ACTION.TASK_TELLER_SAVING.FAILED:
        case ACTION.TASK_TELLER_FINISH.REQUEST:
            return false;
        default:
            return state;
    }
};

let savingNumber = (state = {number: 0}, action) => {
    switch (action.type) {
        case ACTION.TASK_TELLER_SAVING.REQUEST:
            return {};
        case ACTION.TASK_TELLER_SAVING.FAILED:
            return {};
        case ACTION.TASK_TELLER_SAVING.SUCCESS:
            return {...state, number: state['number']+1}
        case ACTION.TASK_TELLER_FINISH.SUCCESS:
            return state;
        default:
            return state;
    }
};

export default combineReducers({
    loading,
    loadingSaving,
    createTaskTeller,
    savingTaskTeller,
    finishTaskTeller,
    savingNumber
});
