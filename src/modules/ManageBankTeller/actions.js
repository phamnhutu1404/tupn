import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const createTaskTeller = createAction(ACTION.CREATE_TASK_TELLER.REQUEST, data => data);
export const taskTellerSaving = createAction(ACTION.TASK_TELLER_SAVING.REQUEST, data => data);
export const taskTellerFinish = createAction(ACTION.TASK_TELLER_FINISH.REQUEST, data => data);
export const clearState = createAction(ACTION.CLEAR_STATE);
