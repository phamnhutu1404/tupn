import {notification} from 'antd';
import {t} from './../../system/i18n';
import {processApiRequest} from './../Common/saga';
import * as ACTION from './constants';
import apiService from './apiService';

export function* createTaskTeller(action) {
    yield processApiRequest(
        ACTION.CREATE_TASK_TELLER,
        () => apiService.createTaskTeller(action.payload),
        action.payload
    );
}

export function* savingTaskTeller(action) {
    yield processApiRequest(
        ACTION.TASK_TELLER_SAVING,
        () => apiService.taskTellerSaving(action.payload),
        action.payload
    );
}

export function* finishTaskTeller(action) {
    yield processApiRequest(
        ACTION.TASK_TELLER_FINISH,
        () => apiService.taskTellerFinish(action.payload),
        action.payload
    );
}

export function* onCreatePackageSameBarcodeSuccess() {
    yield notification.success({message: t('package:create_same_barcode.message_create_package_success')});
}

export function* onCreatePackageSameBarcodeFailed() {
    yield notification.error({message: t('package:create_same_barcode.message_create_package_failed'), duration: 0});
}
