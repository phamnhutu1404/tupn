import React, { useState, useEffect } from "react"
import UserDropDown from './../../../components/UserDropDown/index'
import {Alert, Button, Checkbox, Col, Divider, List, Avatar, Form, Row, Spin} from "antd";
import {PoweroffOutlined, CustomerServiceFilled, UserOutlined, EyeInvisibleFilled} from "@ant-design/icons";
import {BANK_ROOM} from "./../../Login/constants";
import {socket} from "./../../../system/store/index";
const Services = (props) => {
    const [start_teller, setStartTeller] = useState(false)
    const [saving, setSaving] = useState(false)
    const [users, setUsers] = useState([])
    const [userServices, setUserServices] = useState([])
    const [codes, setCode] = useState('')
    const [message, setMessage] = useState('')
    const [messages, setMessages] = useState([])
    const [usersRecover, setUsersRecover] = useState([])
    const [initialValues, setInitialValues] = useState({ open_card: false, deposit: false, withdraw: false, lock_account: false })
    const [messageRecoverPassWord, setMessageRecoverPassWord] = useState([])


    useEffect(() => {
        // socket.emit('join', { name:'333', room : BANK_ROOM }, (error) => {
        //     if(error) {
        //         alert(error);
        //     }
        // });
        socket.emit('joinBank', { name:'333', room : BANK_ROOM }, (error) => {
            if(error) {
                alert(error);
            }
        });
        socket.emit("getUserServices", () => {});
    }, []);

    useEffect(() => {
        socket.on('messageBank', message => {
            setMessages(messages => [ ...messages, message ]);
        });

        socket.on('code', message => {
            setMessages(codes => [ ...codes, message ]);
        });

        socket.on("roomDataBank", ({ users }) => {
            setUsers(users);
        });

        socket.on("roomDataUserServices", (userServices) => {
            setUserServices(userServices);
        });

        props.fetchServices();
    }, []);

    const startWork = (values) => {
        setStartTeller(false)
        setSaving(false)
        props.createTaskTeller(values)
    }

    const startSaving = (values) => {
        setSaving(true)
        props.taskTellerSaving({});
    }

    const startFinish = (values) => {
        setSaving(false)
        props.taskTellerFinish({});
    }

    const { userData = [], listServices=[], loadingSaving, savingNumber = 0, savingTaskTeller={} } = props

    return (
        <div className={'home-content'}>
            <Row>
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <UserDropDown userData={userData}/>
                </Col>
                <Col xs={12} sm={12} md={12} lg={12} xl={11}>
                    <Divider className={'title-category'} orientation="left">
                        List customer ( {userServices.length} )
                    </Divider>
                    <Form initialValues={{ open_card: false, deposit: false, withdraw: false, lock_account: false }}
                          onFinish={()=>{}} name="normal_services">
                        <div>
                            {
                                <List
                                    itemLayout="horizontal"
                                    dataSource={userServices}
                                    renderItem={item => (
                                        <List.Item>
                                            <List.Item.Meta
                                                avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                                title={<b>{'Customer: '+item.name}</b>}
                                                description={ <><CustomerServiceFilled/> {item.services || ""}</>}
                                            />
                                        </List.Item>
                                    )}
                                />
                            }
                        </div>
                        &nbsp;
                    </Form>
                </Col>
                <Col xs={12} sm={12} md={12} lg={12} xl={2}>&nbsp;</Col>
                <Col xs={12} sm={12} md={12} lg={12} xl={11}>
                    <Divider className={'title-category'} orientation="left">
                        Bank teller
                    </Divider>
                    {
                        start_teller ?
                            <Form initialValues={initialValues}
                                  onFinish={()=>{}} name="normal_services">
                                <Row>
                                    <Col span={24}><Alert message="Saving" type="success" /></Col>
                                    <Col span={24}>&nbsp;&nbsp;</Col>
                                    {saving ? <Col span={24}>
                                        Related customer number: {loadingSaving ? <Spin tip="Loading...">
                                        <Alert
                                            message="Connecting"
                                            description="Please wait for the system to connect with the customer"
                                            type="info"
                                        />
                                    </Spin>: null}
                                        <>{savingNumber && savingTaskTeller['id'] ? ' '+savingTaskTeller['id'] : null}</>
                                        <>{savingNumber && savingTaskTeller['id'] ==='' ? ' Does not available customer' : null}</></Col>:""}
                                    &nbsp;
                                    <Col span={24}>
                                        <Form.Item>
                                            <Button type="primary"  icon={<PoweroffOutlined />} loading={loadingSaving} htmlType="button" disabled={loadingSaving} onClick={startSaving} className="login-form-button">
                                                Saving
                                            </Button>
                                            &nbsp;&nbsp;
                                            <Button type="primary" htmlType="button" disabled={!saving} onClick={startFinish} className="login-form-button">
                                                Finish
                                            </Button>
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Form>
                            :
                            <Form initialValues={initialValues} onFinish={startWork} name="normal_services">
                                <Row>
                                    {listServices.map(item=>{
                                        return <React.Fragment key={item.name}><Col span={24} >
                                            <Form.Item name={item.name} key={Math.random()} valuePropName="checked" noStyle>
                                                <Checkbox>{item.label}</Checkbox>
                                            </Form.Item>
                                        </Col>
                                            &nbsp;
                                        </React.Fragment>
                                    })}
                                    &nbsp;
                                    <Col span={24} >
                                        <Form.Item>
                                            <Button type="primary" htmlType="submit" className="login-form-button">
                                                Send service
                                            </Button>
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Form>
                    }

                </Col>
            </Row>
        </div>
    )
}

export default Services