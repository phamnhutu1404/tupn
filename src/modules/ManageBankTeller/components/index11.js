import React from 'react'
import {Row, Col, Divider, Checkbox, Spin, Button, Form, Alert } from 'antd'
import UserDropDown from './../../../components/UserDropDown/index'
import { PoweroffOutlined, LoadingOutlined } from '@ant-design/icons';
import io from "socket.io-client"

let socket;

class Services extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            start_teller: false,
            saving: false,
            finish: false,
            initialValues:{ open_card: false, deposit: false, withdraw: false, lock_account: false },
            users: [],
            messages:'',
        };
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(this.props.listServices) !== JSON.stringify(nextProps.listServices)) {
            const initialValues = Object.assign({}, ...nextProps.listServices.map(p => ({[p.name]: false})));
            this.setState((state, props) => {
                return {...state, initialValues};
            });
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        socket.on("roomData", ({ users }) => {
            console.log(users,'users')
            this.setState((state, props) => {
                return {...state, users};
            });
        });

        socket.on('message', message => {
            let {messages} = this.state
            this.setState((state, props) => {
                return {...state, messages :[ ...messages, message ]};
            });
        });
    }

    componentDidMount() {
        socket = io('http://localhost:9001')

        if( !Object.keys(this.props.userData || {}).length ){
            this.props.getUser();
        }
        this.props.fetchServices();
    }

    startWork = (values) => {
        this.setState((state, props) => {
            return {...state, start_teller: true, saving: false};
        });
        this.props.createTaskTeller(values)
    }

    startSaving = (values) => {
        this.setState((state, props) => {
            return {...state, saving: true};
        });
        this.props.taskTellerSaving({});
    }

    startFinish = (values) => {
        this.setState((state, props) => {
            return {...state, saving: false};
        });
        this.props.taskTellerFinish({});
    }


    render() {

        const { userData = [], listServices=[], loadingSaving, savingNumber = 0, savingTaskTeller={} } = this.props

        const {start_teller, saving = false, users, messages} = this.state

        return (
            <div className={'home-content'}>
                <Row>
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        <UserDropDown userData={userData}/>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12} xl={11}>
                        <Divider className={'title-category'} orientation="left">
                            List customer (55)
                        </Divider>
                        <Form initialValues={{ open_card: false, deposit: false, withdraw: false, lock_account: false }}
                              onFinish={this.submitForm} name="normal_services">
                            {messages.length}asdasasdfasdfasdf
                            {users.length}
                            &nbsp;
                        </Form>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12} xl={2}>&nbsp;</Col>
                    <Col xs={12} sm={12} md={12} lg={12} xl={11}>
                        <Divider className={'title-category'} orientation="left">
                            Bank teller
                        </Divider>
                        {
                            start_teller ?
                                <Form initialValues={this.state.initialValues}
                                      onFinish={()=>{}} name="normal_services">
                                    <Row>
                                        <Col span={24}><Alert message="Saving" type="success" /></Col>
                                        <Col span={24}>&nbsp;&nbsp;</Col>
                                        {saving ? <Col span={24}>
                                            Related customer number: {loadingSaving ? <Spin tip="Loading...">
                                                <Alert
                                                    message="Connecting"
                                                    description="Please wait for the system to connect with the customer"
                                                    type="info"
                                                />
                                            </Spin>: null}
                                            <>{savingNumber && savingTaskTeller['id'] ? ' '+savingTaskTeller['id'] : null}</>
                                            <>{savingNumber && savingTaskTeller['id'] ==='' ? ' Does not available customer' : null}</></Col>:""}
                                        &nbsp;
                                        <Col span={24}>
                                            <Form.Item>
                                                <Button type="primary"  icon={<PoweroffOutlined />} loading={loadingSaving} htmlType="button" disabled={loadingSaving} onClick={this.startSaving} className="login-form-button">
                                                    Saving
                                                </Button>
                                                &nbsp;&nbsp;
                                                <Button type="primary" htmlType="button" disabled={!saving} onClick={this.startFinish} className="login-form-button">
                                                    Finish
                                                </Button>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                </Form>
                                :
                                <Form initialValues={this.state.initialValues} onFinish={this.startWork} name="normal_services">
                                    <Row>
                                        {listServices.map(item=>{
                                            return <React.Fragment key={item.name}><Col span={24} >
                                                <Form.Item name={item.name} key={Math.random()} valuePropName="checked" noStyle>
                                                    <Checkbox>{item.label}</Checkbox>
                                                </Form.Item>
                                            </Col>
                                                &nbsp;
                                            </React.Fragment>
                                        })}
                                        &nbsp;
                                        <Col span={24} >
                                            <Form.Item>
                                                <Button type="primary" htmlType="submit" className="login-form-button">
                                                     Send service
                                                </Button>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                </Form>
                        }

                    </Col>
                </Row>
            </div>
        )
    }
}

export default Services