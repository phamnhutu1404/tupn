import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Home from './components/index'
import {getServices} from './../OptionBank/actions'
import {getUser} from './../Login/actions'
import {createTaskTeller, taskTellerSaving, taskTellerFinish} from './actions'

const mapStateToProps = (state, props) => {
    const {userData = {}} = state.user;
    const {listServices = {}} = state.bankServices
    const {loadingSaving = false, loading, savingTaskTeller={}, savingNumber} = state.manageBankTeller
    return {
        loading,
        userData,
        listServices,
        loadingSaving,
        savingTaskTeller,
        savingNumber
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchServices: (params) => {
            dispatch(getServices(params));
        },
        getUser: (filter) => {
            dispatch(getUser(filter));
        },
        createTaskTeller: (filter) => {
            dispatch(createTaskTeller(filter));
        },
        taskTellerSaving: (filter) => {
            dispatch(taskTellerSaving(filter));
        },
        taskTellerFinish: (filter) => {
            dispatch(taskTellerFinish(filter));
        },
    };
};

let Container = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

Container.defaultProps = {
    input: {},
    canCreate: false,
};

Container.propTypes = {
    input: PropTypes.object,
    canCreate: PropTypes.bool,
};

export default Container;

