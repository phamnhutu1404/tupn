import {api} from '../../system/api';

export default {
    createTaskTeller: (params) => new Promise((resolve, reject) => {
        const data = params
        resolve({error: false, data});
    }),

    taskTellerSaving: (params) => new Promise((resolve, reject) => {
        const data = {id: Math.floor(Math.random() * 100)}
        setTimeout(() => resolve({error: false, data}), 3000)
    }),

    taskTellerFinish: (params) => new Promise((resolve, reject) => {
        const data = params
        resolve({error: false, data});
    }),
    createByBarcode: (params) => api.post('packages/barcode', {...params}, {singleRequest: true}),
    createPackageSameBarcode: (params) => api.post(`packages/barcode/${params.barcode}`, params),
    linkOrderPackage: (packageCode, orderCode) => api.post(`packages/${packageCode}/link-order`, {order_code: orderCode}),
    updateDimension: (packageCode, data) => api.put(`packages/${packageCode}/update-volume`, data),
    updateWeightPackage: (params) => api.put(`packages/${params.packageCode}/update-weight`, {weight_net:params.weight_net}),
    updateRelatedPackage: (id, data) => api.put(`packages/${id}/update`, data),
    getPackages: (params) => api.get('packages', {params}, {singleRequest: true}),
    getPackagePrint: (id) => api.get(`packages/${id}/print-barcode`, {singleRequest: true}),
    getPackage: (id) => api.get(`packages/${id}`, {singleRequest: true}),
};
