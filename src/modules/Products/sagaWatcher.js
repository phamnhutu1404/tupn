import {all, fork} from 'redux-saga/effects';
import listProductsWatcher from './ListProducts/sagaWatcher';
import detailWatcher from './Detail/sagaWatcher';
import categoryProductWatcher from './Category/sagaWatcher';

export default function*() {
    yield all([
        fork(listProductsWatcher),
        fork(detailWatcher),
        fork(categoryProductWatcher),
    ]);
}