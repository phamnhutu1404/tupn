import {api} from '../../system/api';

export default {

    createProduct: (params) => api.post(`packages/barcode/${params.barcode}`, params),
    getPackagePrint: (id) => api.get(`packages/${id}/print-barcode`, {singleRequest: true}),
    //getProductBrand: (id) => api.get(`packages/${id}`, {singleRequest: true}),
    getProductBrand: (id) => new Promise((resolve, reject) => {
        const data = {
            paging:{
                page: 1, pageSize: 10, totalPage: 2, totalRows : 1000
            },
            category:{id:1, title:'Giay cong so 11'},
            listProduct: [ {id:1, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:2, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:3, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:4, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:5, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:6, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:7, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:8, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:9, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:10, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:11, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:12, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:13, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:14, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:15, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:16, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"}]
        }
        resolve({error: false, data});
    }),
    getProductDetail: (id) => new Promise((resolve, reject) => {
        const data = {
            paging:{
                page: 1, pageSize: 10, totalPage: 2, totalRows : 1000
            },
            category:{id:1, title:'Giay cong so 11'},
            listProduct: [ {id:1, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:2, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:3, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:4, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:5, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:6, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:7, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:8, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:9, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:10, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:11, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:12, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:13, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:14, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:15, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
                {id:16, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"}]
        }
        resolve({error: false, data});
    }),
};
