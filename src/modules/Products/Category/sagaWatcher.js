import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.GET_PRODUCT_BRAND.REQUEST, saga.onFetchProductBrand);
    yield takeLatest(ACTION.GET_PRODUCT_BRAND.SUCCESS, saga.onCreatePackageSameBarcodeSuccess);
    yield takeLatest(ACTION.GET_PRODUCT_BRAND.FAILED, saga.onCreatePackageSameBarcodeFailed);
}
