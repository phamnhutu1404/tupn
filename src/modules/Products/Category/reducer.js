import {combineReducers} from 'redux';
import * as ACTION from './constants';

let loading = (state = false, action) => {
    switch (action.type) {
        case ACTION.GET_PRODUCT_BRAND.REQUEST:
            return true;

        case ACTION.GET_PRODUCT_BRAND.SUCCESS:
        case ACTION.GET_PRODUCT_BRAND.FAILED:
        case ACTION.CLEAR_STATE:
            return false;

        default:
            return state;
    }
};

let brandListProduct = (state = [], action) => {
    switch (action.type) {
        case ACTION.GET_PRODUCT_BRAND.REQUEST:
            return [];
        case ACTION.GET_PRODUCT_BRAND.FAILED:
        case ACTION.CLEAR_STATE:
            return [];
        case ACTION.GET_PRODUCT_BRAND.SUCCESS:
            return action.payload
        default:
            return state;
    }
};

let paging = (state = [], action) => {
    switch (action.type) {
        case ACTION.GET_PRODUCT_BRAND.REQUEST:
            return [];
        case ACTION.GET_PRODUCT_BRAND.FAILED:
        case ACTION.CLEAR_STATE:
            return [];
        case ACTION.GET_PRODUCT_BRAND.SUCCESS:
            return action.payload.paging
        default:
            return state;
    }
};

export default combineReducers({
    loading,
    brandListProduct,
    paging
});
