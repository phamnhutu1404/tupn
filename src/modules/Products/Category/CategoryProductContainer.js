import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getProductDetail} from './actions';
import DetailComponent from './components/index';

const mapStateToProps = (state, props) => {
    const {productBrand} = state.products
    return {
        products: productBrand.brandListProduct.listProduct || [],
        category: productBrand.brandListProduct.category || {},
        loading: productBrand.loading || false,
        paging: productBrand.paging || {},
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchProductDetail: (filter) => {
            dispatch(getProductDetail(filter.id));
        },
    };
};

let DetailProductContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DetailComponent);

DetailProductContainer.defaultProps = {
    input: {},
    canCreate: false,
};

DetailProductContainer.propTypes = {
    input: PropTypes.object,
    canCreate: PropTypes.bool,
};

export default DetailProductContainer;

