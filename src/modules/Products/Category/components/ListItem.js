import React from 'react'
import {Col} from "antd"
import {Link} from './../../../../system/routing'

class ListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    render() {
        const {item} = this.props;
        return (
            <Col span={4} xs={24} sm={12} md={8} lg={8} xl={6} xxl={4} className={'gutter-row home-content__item-product'}>
                <Link to="detail.product" params={{id: item.id}} alt={item.title} key={item.id}>
                    <div className={'image-info'}>
                        <img className={'image-product'} src={item.src} alt={item.title}/>
                    </div>
                    <div className={'product-price'}>
                        <div className="price2">
                            <span>650.000</span>
                            <span>₫</span>
                        </div>
                        <div className="price1">
                            <span>650.000</span>
                            <span>₫</span>
                        </div>
                    </div>
                    <div className={'product-name'}>
                        {item.title} {item.id}
                    </div>
                </Link>
            </Col>
        );
    }
}

export default ListItem;
