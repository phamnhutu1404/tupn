import React from 'react';
import ReactDOM from 'react-dom';
import { PDFExport, savePDF } from '@progress/kendo-react-pdf';
import { Grid, GridColumn as Column } from '@progress/kendo-react-grid';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { Row, Divider } from 'antd';
import {url,router,Link} from './../../../../system/routing/index';
import ListItem from './ListItem'

class BlockCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        const {category, data=[]} = this.props
        return (
            <React.Fragment>
                <Divider className={'title-category'} orientation="left">
                    <Link to={'category.product'} params={{id: category.id}}>{category.title}</Link>
                </Divider>
                <Row gutter={16}>
                    {data.map(item=>{return <ListItem item={item} key={item.id}/>})}
                </Row>
            </React.Fragment>
        );
    }
}

export default BlockCategory;
