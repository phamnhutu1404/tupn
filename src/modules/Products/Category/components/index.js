import React, {Component} from 'react';
import {translate} from 'react-i18next';
import queryString from 'query-string';
import lodash from 'lodash';
import PropTypes from 'prop-types';
import {Link} from './../../../../system/routing'
import {
    Layout, Menu, Breadcrumb, Icon, Carousel, Row, Col,
} from 'antd';

import BlockItem from './Block';
import Pagination from './../../../Common/components/Pagination';
const REACT_VERSION = React.version;
const data =      {
         category:{id:1, title:'Giay cong so 2'},
         listProduct: [ {id:1, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:2, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:3, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:4, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:5, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:6, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:7, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:8, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:9, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:10, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:11, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:12, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:13, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:14, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:15, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
             {id:16, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"}]
     }
class App extends Component {
    componentDidMount() {

        const {location, match} = this.props;
        let filters = {...queryString.parse(location.search), ...match.params};
        this.props.fetchProductDetail(filters);
    }

    render() {
        const {category, products = [], loading, paging} = this.props
        return (
            <>
                <Row gutter={16}>
                    <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                        <Breadcrumb>
                            <Breadcrumb.Item>
                                <Link to={'home'}>Home</Link>
                            </Breadcrumb.Item>
                            <Breadcrumb.Item>{category.title}</Breadcrumb.Item>
                        </Breadcrumb>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>Category {REACT_VERSION}</Col>
                </Row>
                <BlockItem category={category} data={products || []}/>
                <Row xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                    <Pagination page={1}
                    pageSize= {10}
                    total= {1000}
                    showSizeChanger= {false}
                    pageSizeOptions= {[5,3,4]}
                    loading= {false}
                    onChange= {()=>{}}/>
                </Row>
            </>
        );
    }
}

export default App;
