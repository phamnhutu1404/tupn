import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const getProductDetail = createAction(ACTION.GET_PRODUCT_BRAND.REQUEST, data => data);
export const clearState = createAction(ACTION.CLEAR_STATE);
