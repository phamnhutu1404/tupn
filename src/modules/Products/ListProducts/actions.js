import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const getListProducts = createAction(ACTION.GET_LIST_PRODUCT.REQUEST);
export const clearState = createAction(ACTION.CLEAR_STATE);
