import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getListProducts} from './actions';
import ListProduct from './components/index';
import SearchableComponent from '../../Common/components/SearchableComponent';

const mapStateToProps = (state, props) => {
    return {};
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchListProducts: (filter) => {
            dispatch(getListProducts(filter));
        },
    };
};

class ListPackageContainer extends SearchableComponent {
    onSearch(filter) {
        this.pushFilter({
            ...filter,
            i: parseInt(filter.i || 0, 0) + 1,
        });
    }

    onChangeFilter(filter) {
        this.props.fetchListProducts(filter);
    }

    getCurrentFilter() {
        return {
            page: 1,
            ...this.getFilterFromLocation(this.props.location),
        };
    }

    render() {
        return <ListProduct {...{
            ...this.props,
            filter: this.getCurrentFilter(),
            onSearch: this.onSearch.bind(this),
        }}/>
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ListPackageContainer)

