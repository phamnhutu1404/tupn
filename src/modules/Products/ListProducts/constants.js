export const GET_LIST_PRODUCT = {
    REQUEST: 'PACKAGE.GET_LIST_PRODUCT.REQUEST',
    SUCCESS: 'PACKAGE.GET_LIST_PRODUCT.SUCCESS',
    FAILED: 'PACKAGE.GET_LIST_PRODUCT.FAILED',
};

export const CLEAR_STATE = 'PACKAGE.GET_LIST_PRODUCT.CLEAR_STATE';
