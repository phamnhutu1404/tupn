import React, {Component} from 'react';

import {
    Layout, Menu, Breadcrumb, Icon, Carousel, Row, Col,
} from 'antd';
import ListItem from './ListItem';

class App extends Component {
    render() {
        return (
            <Row gutter={16}>
                {[1, 2, 3, 4, 5].map(item =>
                    <ListItem data={item}/>
                )}
            </Row>
        );
    }
}

export default App;
