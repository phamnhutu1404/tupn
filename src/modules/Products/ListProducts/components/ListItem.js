import React, {Component} from 'react';
import {Link} from '../../../../system/routing';
import {
    Layout, Menu, Breadcrumb, Icon, Carousel, Row, Col,
} from 'antd';


class App extends Component {
    render() {
        let {data} = this.props;
        return (
            <Col span={6}>
                <Link to="detail.product" params={{id:data}}>Product {data}</Link>
            </Col>
        );
    }
}

export default App;
