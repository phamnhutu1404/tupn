import {combineReducers} from 'redux';
import listProducts from './ListProducts/reducer';
import detailProducts from './Detail/reducer';
import productBrand from './Category/reducer';

export default combineReducers({
    listProducts,
    orderDetail: detailProducts,
    productBrand: productBrand
});