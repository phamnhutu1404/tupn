import {createAction} from 'redux-actions';
import * as ACTION from './constants';
import * as CART_ACTION from './../../Cart/constants';

export const getProductDetail = createAction(ACTION.GET_ORDER_DETAIL.REQUEST, data => data);

export const clearState = createAction(ACTION.CLEAR_STATE);
