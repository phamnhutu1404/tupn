import React, {Component} from 'react'
import {translate} from 'react-i18next'
import queryString from 'query-string'
import ImageGallery from 'react-image-gallery'
import "react-image-gallery/styles/css/image-gallery.css";
import {url,Link, router} from './../../../../system/routing'
import {
    Radio , InputNumber,  Divider, Button,Breadcrumb, Row, Col,
} from 'antd'

import {MinusOutlined, PlusOutlined, ShoppingCartOutlined} from '@ant-design/icons'
import {MAX_QUANTITY, MIN_QUANTITY} from "./../../../Cart/constants"
// import {t} from "src/system/i18n";
const ButtonGroup = Button.Group


const data = {id:1, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"}
const minQuantity = 1
const maxQuantity = 99
const sizes = [35,36,37,38,39]
const colors = ['đỏ','vàng', 'trắng','xanh']
const images = [
    {
        original: 'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6',
        thumbnail: 'https://cf.shopee.vn/file/206f8eef07a917fa3c844b4dd9b6abb3',
    },
    {
        original: 'https://cf.shopee.vn/file/b8f90153334c3c48b6940d85bf108abe',
        thumbnail: 'https://cf.shopee.vn/file/b8f90153334c3c48b6940d85bf108abe',
    },
    {
        original: 'https://cf.shopee.vn/file/206f8eef07a917fa3c844b4dd9b6abb3',
        thumbnail: 'https://cf.shopee.vn/file/206f8eef07a917fa3c844b4dd9b6abb3',
    },
];
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {idProduct: data.id, quantity: 1, sizeProduct:'',color:''};
    }

    componentDidMount() {
        const {location, match} = this.props;
        let filters = {...queryString.parse(location.search), ...match.params};
        this.props.fetchProductDetail(filters);
        this.props.getListCart();
    }

    //choose color
    onChangeColor = (e)=>{
        this.setState({color: e.target.value})
    }

    //choose color
    onChangeSize = (e)=>{
        this.setState({sizeProduct: e.target.value})
    }
    // onChange Quantity
    onChangeQuantity = (value)=>{
        if(Number.isInteger(value)){
            this.setState({quantity: value})
        }
    }

    //decline Quantity
    declineQuantity = ()=>{
        const {quantity} = this.state
        if( MIN_QUANTITY < quantity){
            this.setState({quantity: quantity-1})
        }
    }
    // increase Quantity
    increaseQuantity = ()=>{
        const {quantity} = this.state
        if( quantity<MAX_QUANTITY){
            this.setState({quantity: quantity+1})
        }
    }
    //add product to cart
    submitToCart = ()=>{
        const {idProduct, quantity, sizeProduct, color} = this.state
        this.props.createProductToCart({idProduct, quantity, sizeProduct, color})
        router.redirect(url.to('cart.product'))
    }
    //buy detail product
    cartToBuy = ()=>{

    }

    render() {
        const {title}= data
        const {quantity, sizeProduct, color} = this.state
        return (
            <div className={'product-content'}>
                <Row gutter={16}>
                    <Col xs={24} sm={8} md={8} lg={8} xl={8} xxl={8} className={'product-content__info-product'}>
                        <Breadcrumb>
                            <Breadcrumb.Item>
                                <Link to={'home'}>Trang chu</Link>
                            </Breadcrumb.Item>
                            <Breadcrumb.Item><Link to={'category.product'} params={{id:2}}>Giay cao got</Link></Breadcrumb.Item>
                            <Breadcrumb.Item>{data.title}</Breadcrumb.Item>
                        </Breadcrumb>
                        <ImageGallery items={images} showPlayButton={false} additionalClass={'album-product-detail'}/>
                    </Col>
                    <Col xs={24} sm={16} md={16} lg={16} xl={16} xxl={16} className={'product-content__info-product product-content__info-product-content'}>
                        <div className={'product-title'}>{title}</div>
                        <div className={'product-price'}>
                            <div className="price2">
                                <span>650.000</span>
                                <span>₫</span>
                            </div>
                            <div className="price1">
                                <span>650.000</span>
                                <span>₫</span>
                            </div>
                        </div>
                        {colors.length? <div className={'product-size'}>
                            <Divider className={'title-category'} orientation="left">
                                {'Màu sắc'}
                            </Divider>
                            <Radio.Group onChange={this.onChangeColor.bind(this)} name={'color'} value={color}>
                                {colors.map(itemSize=>
                                    <Radio value={itemSize} key={itemSize}>{itemSize}</Radio>
                                )}
                            </Radio.Group>
                        </div>:null}

                        {sizes.length? <div className={'product-size'}>
                            <Divider className={'title-category'} orientation="left">
                                {'Size'}
                            </Divider>
                            <Radio.Group onChange={this.onChangeSize.bind(this)} defaultValue={sizeProduct} name={'size'} className={'option-size'}>
                                {sizes.map(itemSize=>
                                    <Radio.Button value={itemSize} key={itemSize}>{itemSize}</Radio.Button>
                                )}
                            </Radio.Group>
                        </div>:null}

                        <div className={'product-size'}>
                            <Divider className={'title-category'} orientation="left">
                                {'Số Lượng'}
                            </Divider>
                            <div className="ant-btn-group">
                                <ButtonGroup>
                                    <Button onClick={this.declineQuantity.bind(this)}>
                                        <MinusOutlined />
                                    </Button>
                                    <InputNumber className={'quantity-product'} min={MIN_QUANTITY} max={MAX_QUANTITY}
                                                 value={quantity} defaultValue={quantity} onChange={this.onChangeQuantity.bind(this)} />
                                    <Button onClick={this.increaseQuantity.bind(this)}>
                                        <PlusOutlined />
                                    </Button>
                                </ButtonGroup>
                            </div>
                        </div>
                        <br/>
                        <p></p>
                        <Button type="primary" onClick={this.submitToCart.bind(this)}>
                             Thêm vào giỏ hàng <ShoppingCartOutlined />
                             {/*{t('package:create_same_barcode.message_create_package_success')}*/}
                        </Button>
                        &nbsp;&nbsp;
                        <Button type="danger" onClick={this.cartToBuy.bind(this)}>
                            Mua hang ngay
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default App;
