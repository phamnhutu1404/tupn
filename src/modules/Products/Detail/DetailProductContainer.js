import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getProductDetail} from './actions';
import DetailComponent from './components/index';
import {getListCart, createProductToCart, updateProductToCart, removeProductToCart} from './../../Cart/actions'
import {createAction} from "redux-actions"

const mapStateToProps = (state, props) => {
    return {
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchProductDetail: (filter) => {
            dispatch(getProductDetail(filter.id));
        },
        getListCart: ()=>{
            dispatch(getListCart())
        },
        createProductToCart: (filter)=>{
            dispatch(createProductToCart(filter))
        }
    };
};

let DetailProductContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DetailComponent);

DetailProductContainer.defaultProps = {
    input: {},
    canCreate: false,
};

DetailProductContainer.propTypes = {
    input: PropTypes.object,
    canCreate: PropTypes.bool,
};

export default DetailProductContainer;

