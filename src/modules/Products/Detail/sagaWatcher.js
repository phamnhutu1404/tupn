import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.GET_ORDER_DETAIL.REQUEST, saga.onFetchProductDetail);
    yield takeLatest(ACTION.GET_ORDER_DETAIL.SUCCESS, saga.onCreatePackageSameBarcodeSuccess);
    yield takeLatest(ACTION.GET_ORDER_DETAIL.FAILED, saga.onCreatePackageSameBarcodeFailed);
}
