import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const postLogin = createAction(ACTION.LOGIN_SYSTEM.REQUEST, data => data);
export const getUser = createAction(ACTION.GET_ME_ACCOUNT.REQUEST, data => data);
export const logoutUser = createAction(ACTION.LOGOUT_SYSTEM.REQUEST, data => data);
export const clearState = createAction(ACTION.CLEAR_STATE);
