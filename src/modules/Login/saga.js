import {notification} from 'antd';
import {t} from './../../system/i18n';
import {processApiRequest} from './../Common/saga';
import * as ACTION from './constants';
import apiService from './apiService';

export function* postLogin(action) {
    yield processApiRequest(
        ACTION.LOGIN_SYSTEM,
        () => apiService.postLogin(action.payload),
        action.payload
    );
}

export function* getUser(action) {
    yield processApiRequest(
        ACTION.GET_ME_ACCOUNT,
        () => apiService.getUser(action.payload),
        action.payload
    );
}

export function* postLogout(action) {
    yield processApiRequest(
        ACTION.LOGOUT_SYSTEM,
        () => apiService.postLogout(action.payload),
        action.payload
    );
}


export function* onCreatePackageSameBarcodeSuccess() {
    yield notification.success({message: t('package:create_same_barcode.message_create_package_success')});
}

export function* onCreatePackageSameBarcodeFailed() {
    yield notification.error({message: t('package:create_same_barcode.message_create_package_failed'), duration: 0});
}