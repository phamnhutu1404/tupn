import {combineReducers} from 'redux';
import * as ACTION from './constants';

let loading = (state = false, action) => {
    switch (action.type) {
        case ACTION.LOGIN_SYSTEM.REQUEST:
            return true;

        case ACTION.LOGIN_SYSTEM.SUCCESS:
        case ACTION.LOGIN_SYSTEM.FAILED:
        case ACTION.CLEAR_STATE:
            return false;

        default:
            return state;
    }
};

let userData = (state = {}, action) => {
    switch (action.type) {
        case ACTION.LOGIN_SYSTEM.REQUEST:
        case ACTION.GET_ME_ACCOUNT.REQUEST:
            return {};
        case ACTION.LOGIN_SYSTEM.FAILED:
        case ACTION.GET_ME_ACCOUNT.FAILED:
        case ACTION.CLEAR_STATE:
            return {};
        case ACTION.LOGIN_SYSTEM.SUCCESS:
        case ACTION.GET_ME_ACCOUNT.SUCCESS:
        case ACTION.LOGOUT_SYSTEM.SUCCESS:
            return action.payload
        default:
            return state;
    }
};

export default combineReducers({
    loading,
    userData,
});
