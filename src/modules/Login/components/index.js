import React from 'react'
import {Row, Button, Col} from 'antd'
import { Form, Input, Checkbox, Select } from 'antd'
import { UserOutlined, EyeInvisibleFilled } from '@ant-design/icons'
import {router, url} from "./../../../system/routing";
const { Option } = Select;

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectionType: []
        };
    }

    componentDidMount() {
        this.props.getUser()
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(nextProps.userData['username']){
            if( 'customer'.localeCompare(nextProps.userData['role']) === 0 ){
                router.redirect(url.to('bank.services'))
            }
            else{
                router.redirect(url.to('manage.bank.teller'))

            }
        }
        if( JSON.stringify(this.props.userData) === JSON.stringify(nextProps.userData)) {
            return true
        }
        else {
            return false
        }
    }

    onFinish = (values) => {
        this.props.postLogin({username: values['username'], password: values['password'], role: values['role'], room:''});
        this.setState({username: values['username'], password: values['password'], role: values['role']})
    }

    render() {

        return (
            <div className={'cart-content'}>
                <Row>
                <Col xs={2} sm={4} md={6} lg={8} xl={10}/>
                <Col xs={20} sm={16} md={12} lg={8} xl={4}>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{ remember: true }}
                        onFinish={this.onFinish}
                    >
                        <Form.Item
                            name="username"
                            rules={[{ required: true, message: 'Please input your username!' }]}
                        >
                            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="ID passport" />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Please input your password!' }]}
                        >
                            <Input type={'password'} prefix={<EyeInvisibleFilled className="site-form-item-icon" />} placeholder="ID passport" />
                        </Form.Item>

                        <Form.Item name="role" defaultValue="customer" label="Role" rules={[{ required: true }]}>
                            <Select
                                placeholder="Select a option role"
                                allowClear
                            >
                                <Option value="customer">Customer</Option>
                                <Option value="bank_teller">Bank teller</Option>
                            </Select>
                        </Form.Item>

                        <Form.Item>
                            <Form.Item name="remember" valuePropName="checked" noStyle>
                                <Checkbox>Remember me</Checkbox>
                            </Form.Item>

                            <a className="login-form-forgot" href="">
                                Forgot password
                            </a>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                Log in
                            </Button>
                            &nbsp; Or <a href="">register now!</a>
                        </Form.Item>
                    </Form>
                </Col>
                <Col xs={2} sm={4} md={6} lg={8} xl={10}/>
                </Row>
            </div>
        );
    }
}

export default Login;
