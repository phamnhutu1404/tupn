import {connect} from 'react-redux';
import {postLogin, getUser, logoutUser} from './actions';
import CartContainer from './components/index';

const mapStateToProps = (state, props) => {
    const {loading, userData} = state.user
    return {
        loading,
        userData
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        postLogin: (props)=>{
            dispatch(postLogin(props))
        },
        getUser: (props)=>{
            dispatch(getUser(props))
        },
        logoutUser: (props)=>{
            dispatch(logoutUser(props))
        },
    };
};

let Container = connect(
    mapStateToProps,
    mapDispatchToProps
)(CartContainer);

export default Container;

