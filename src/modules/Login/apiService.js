import {api} from '../../system/api';
import {saveLocalStorage, getLocalStorage, removeLocalStorage } from '../../utils';
import {CUSTOMER_SYSTEM, BANK_ROOM} from './constants'
import {url,router,Link} from './../../system/routing/index';
import {socket} from "./../../system/store/index";

export default {
    postLogin: (params) => new Promise((resolve, reject) => {

        let data = {username: params['username'], password: params['password'], role: params['role']}
        saveLocalStorage(CUSTOMER_SYSTEM, JSON.stringify(data))
        resolve({error: false, data});
    }),
    getUser: () => new Promise((resolve, reject) => {
        const data = getLocalStorage(CUSTOMER_SYSTEM)
        if( !Object.keys(data).length ){
            router.redirect(url.to('bank.login'))
        }
        return resolve({error: false, data});
    }),

    postLogout: (params) => new Promise((resolve, reject) => {
        removeLocalStorage(CUSTOMER_SYSTEM)
        if( params['role'] === 'customer' ){
            socket.emit('logoutBank', {...params, username: params.username, room: BANK_ROOM}, (error) => {
                if(error) {
                    //alert(error);
                }
            });
        }
        resolve({error: false, data: {}});
    }),
    getBarcodePackages: (barcode) => api.get(`packages/barcode/${barcode}`, {singleRequest: true}),
    createByBarcode: (params) => api.post('packages/barcode', {...params}, {singleRequest: true}),
    linkOrderPackage: (packageCode, orderCode) => api.post(`packages/${packageCode}/link-order`, {order_code: orderCode}),
    updateDimension: (packageCode, data) => api.put(`packages/${packageCode}/update-volume`, data),
    updateWeightPackage: (params) => api.put(`packages/${params.packageCode}/update-weight`, {weight_net:params.weight_net}),
    updateRelatedPackage: (id, data) => api.put(`packages/${id}/update`, data),
    getPackages: (params) => api.get('packages', {params}, {singleRequest: true}),
    getPackagePrint: (id) => api.get(`packages/${id}/print-barcode`, {singleRequest: true}),
    getPackage: (id) => api.get(`packages/${id}`, {singleRequest: true}),
};
