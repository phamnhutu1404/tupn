import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.LOGIN_SYSTEM.REQUEST, saga.postLogin)
    yield takeLatest(ACTION.GET_ME_ACCOUNT.REQUEST, saga.getUser)
    yield takeLatest(ACTION.LOGOUT_SYSTEM.REQUEST, saga.postLogout)
    // yield takeLatest(ACTION.LOGIN_SYSTEM.SUCCESS, saga.onCreatePackageSameBarcodeSuccess)
    // yield takeLatest(ACTION.LOGIN_SYSTEM.FAILED, saga.onCreatePackageSameBarcodeFailed)
    // yield takeLatest(ACTION.UPDATE_PRODUCT_TO_CART.REQUEST, saga.updateCart)
}
