import {combineReducers} from 'redux';
import * as ACTION from './constants';

let loading = (state = false, action) => {
    switch (action.type) {
        case ACTION.GET_INTRO.REQUEST:
            return true;

        case ACTION.GET_INTRO.SUCCESS:
        case ACTION.GET_INTRO.FAILED:
        case ACTION.CLEAR_STATE:
            return false;

        default:
            return state;
    }
};

export default combineReducers({
    loading,
});
