import {api} from '../../system/api';

export default {
    getBarcodePackages: (barcode) => api.get(`packages/barcode/${barcode}`, {singleRequest: true}),
    createByBarcode: (params) => api.post('packages/barcode', {...params}, {singleRequest: true}),
    createPackageSameBarcode: (params) => api.post(`packages/barcode/${params.barcode}`, params),
    linkOrderPackage: (packageCode, orderCode) => api.post(`packages/${packageCode}/link-order`, {order_code: orderCode}),
    updateDimension: (packageCode, data) => api.put(`packages/${packageCode}/update-volume`, data),
    updateWeightPackage: (params) => api.put(`packages/${params.packageCode}/update-weight`, {weight_net:params.weight_net}),
    updateRelatedPackage: (id, data) => api.put(`packages/${id}/update`, data),
    getPackages: (params) => api.get('packages', {params}, {singleRequest: true}),
    getPackagePrint: (id) => api.get(`packages/${id}/print-barcode`, {singleRequest: true}),
    getPackage: (id) => api.get(`packages/${id}`, {singleRequest: true}),
};
