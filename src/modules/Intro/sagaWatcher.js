import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.GET_INTRO.REQUEST, saga.createPackageSameBarcode);
    yield takeLatest(ACTION.GET_INTRO.SUCCESS, saga.onCreatePackageSameBarcodeSuccess);
    yield takeLatest(ACTION.GET_INTRO.FAILED, saga.onCreatePackageSameBarcodeFailed);
}
