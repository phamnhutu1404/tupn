import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.GET_PRODUCT_HOME.REQUEST, saga.getListProduct);
    // yield takeLatest(ACTION.GET_PRODUCT_HOME.SUCCESS, saga.onCreatePackageSameBarcodeSuccess);
    // yield takeLatest(ACTION.GET_PRODUCT_HOME.FAILED, saga.onCreatePackageSameBarcodeFailed);
}