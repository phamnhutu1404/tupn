import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Home from './components/index'
import {getListProduct} from './actions'
import {getUser} from './../Login/actions'

const mapStateToProps = (state, props) => {
    const {userData = {}} = state.user;
    const {loading, createRequest = {}} = state.bankServices
    return {
        loading,
        userData,
        createRequest
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchProduct: (filter) => {
            dispatch(getListProduct(filter));
        },
        getUser: (filter) => {
            dispatch(getUser(filter));
        },
    };
};

let Container = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

Container.defaultProps = {
    input: {},
    canCreate: false,
};

Container.propTypes = {
    input: PropTypes.object,
    canCreate: PropTypes.bool,
};

export default Container;

