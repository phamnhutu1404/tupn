import React from 'react';
import ReactDOM from 'react-dom';
import { PDFExport, savePDF } from '@progress/kendo-react-pdf';
import { Grid, GridColumn as Column } from '@progress/kendo-react-grid';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import {Row, Col, Divider, Checkbox, Button, Form} from 'antd';
import UserDropDown from './../../../components/UserDropDown/index';

class Services extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.props.getUser();
    }

    submitForm = (values) => {
        console.log(values,'values')
    }

    render() {
        const {userData = [], createRequest={}} = this.props
        {console.log(createRequest,'createRequest')}
        return (
            <div className={'home-content'}>
                <Row>
                    <Col xs={2} sm={4} md={6} lg={8} xl={10}>
                        <UserDropDown userData={userData}/>
                    </Col>
                    <Col xs={20} sm={16} md={12} lg={8} xl={4}>
                        <Divider className={'title-category'} orientation="left">
                            Services code
                        </Divider>
                        <Form initialValues={{ open_card: false, deposit: false, withdraw: false, lock_account: false }}
                              onFinish={this.submitForm} name="normal_services">
                            Code number: <b>{createRequest['code_request'] || null}</b>
                            &nbsp;
                        </Form>
                    </Col>
                    <Col xs={2} sm={4} md={6} lg={8} xl={10}/>
                </Row>
            </div>
        )
    }
}

export default Services