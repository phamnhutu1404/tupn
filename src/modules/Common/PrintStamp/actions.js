import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const printPackageStamp = createAction(ACTION.PRINT_PACKAGE_STAMP, params => params);
export const clearPrintState = createAction(ACTION.CLEAR_PRINT_STATE, () => ({}));
