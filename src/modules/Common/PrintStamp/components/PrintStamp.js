import React from 'react';
import PrintStampPackage from './PrintStampPackage';
import {TYPE_PRINT} from './../constants';
import PropTypes from 'prop-types';

class PrintStamp extends React.Component {
    componentDidUpdate() {
        if (this.props.dataPrint.typePrint) {
            window.print();
        }
    }

    render() {
        let {dataPrint} = this.props;
        let {packageData, warehouseCode, typePrint, numberStamp} = dataPrint;

        switch (typePrint) {
            case TYPE_PRINT.PACKAGE:
                return (
                    <PrintStampPackage
                        title={document.title}
                        warehouseCode={warehouseCode}
                        packageData={packageData}
                        numberStamp={numberStamp}
                        classPrint={'stamp-print'}
                    />
                );

            default:
                return null;
        }
    }
}

PrintStamp.defaultProps = {
    dataPrint: {},
};

PrintStamp.propTypes = {
    dataPrint: PropTypes.object,
};

export default PrintStamp;