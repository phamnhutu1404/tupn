import React from 'react';
import lodash from 'lodash';
import {withTranslation} from 'react-i18next';
import Barcode from 'react-barcode';
import PropTypes from 'prop-types';

class PrintStampPackage extends React.Component {

    componentDidUpdate(prevProps, prevState, prevContext) {
        this.scrollTo('a-stamp--print');
    }

    scrollTo(id, callback) {
        let timer = 500;
        let settings = {
            duration: 1000,
            easing: {
                outQuint: function (x, t, b, c, d) {
                    return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
                }
            }
        };
        let percentage;
        let startTime;
        let node = document.getElementById(id);
        let nodeTop = node.offsetTop;
        let nodeHeight = node.offsetHeight;
        let body = document.body;
        let html = document.documentElement;
        let height = Math.max(
            body.scrollHeight,
            body.offsetHeight,
            html.clientHeight,
            html.scrollHeight,
            html.offsetHeight
        );
        let windowHeight = window.innerHeight;
        let offset = window.pageYOffset;
        let delta = nodeTop - offset;
        let bottomScrollableY = height - windowHeight;
        let targetY = (bottomScrollableY < delta) ?
            bottomScrollableY - (height - nodeTop - nodeHeight + offset) :
            delta;

        startTime = Date.now();
        percentage = 0;

        if (timer) {
            clearInterval(timer);
        }

        function step() {
            let yScroll;
            let elapsed = Date.now() - startTime;

            if (elapsed > settings.duration) {
                clearTimeout(timer);
            }

            percentage = elapsed / settings.duration;

            if (percentage > 1) {
                clearTimeout(timer);

                if (callback) {
                    callback();
                }
            } else {
                yScroll = settings.easing.outQuint(0, elapsed, offset, targetY, settings.duration);
                window.scrollTo(0, yScroll);
                timer = setTimeout(step, 10);
            }
        }

        timer = setTimeout(step, 10);
    }

    render() {
        let {packageData, classPrint, numberStamp, warehouseCode} = this.props;
        let {properties = [], services = []} = packageData;
        let ListProperties = properties.map(propertieItem => lodash.isNil(propertieItem.alias) ? '' : propertieItem.alias);
        let Listservices = services.map(serviceItem => lodash.isNil(serviceItem.alias) ? '' : serviceItem.alias);
        ListProperties = lodash.compact(ListProperties);
        Listservices = lodash.compact(Listservices);

        return (
            lodash.range(numberStamp).map((item, index) => (
                <div key={index}>
                    {index ? <div className="a-stamp--print-page-break">{''}</div> : ''}
                    <div className={`a-stamp ${classPrint}`}>
                        <div id="a-stamp--print">
                            <div className="a-stamp--print--top">
                                <div className="a-stamp--warehouse">
                                    <span>{warehouseCode}&nbsp;</span>
                                    <span className="ml-2">
                                        {lodash.join(ListProperties, '')}
                                        {ListProperties.length && Listservices.length ? '-' : ''}
                                        {lodash.join(Listservices, '')}
                                    </span>
                                </div>
                                <div className="a-stamp--package-code"> {packageData.code}</div>
                            </div>
                            <div className="a-stamp--barcode">
                                {packageData.code ? <Barcode
                                    width={2.4}
                                    height={50}
                                    displayValue={false}
                                    value={packageData.code}
                                    copyStyles={true}
                                /> : ''}
                            </div>
                            <div className="a-stamp--print--bototm">
                                <div className="a-stamp--print--block">
                                    <div className="username">
                                        {packageData.order ?
                                            lodash.get(packageData, 'order.customer_username', '')
                                            :
                                            lodash.get(packageData, 'customer_receive', '')
                                        }
                                    </div>
                                    <div className="weight">
                                        <span>
                                            {packageData.weight_net ? packageData.weight_net + "Kg" : ''}
                                            {packageData.weight_converted ? " - QĐ: " + packageData.weight_converted + "Kg" : ''}
                                        </span>
                                    </div>
                                </div>
                                <div className="a-stamp--print--block">
                                    <div className="bill-code">
                                        {packageData.order ? packageData.order.code : ''}
                                    </div>
                                    {(packageData.volume && packageData.volume !== 0) ?
                                        <div className="volumn">{packageData.volume}
                                            <span className="ml-1">m<sup>3</sup></span>
                                        </div>
                                        :
                                        null
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ))
        )
    }
}

PrintStampPackage.defaultProps = {
    title: "",
    warehouseCode: "",
    packageData: {},
    numberStamp: 0,
    classPrint: "",
};

PrintStampPackage.propTypes = {
    title: PropTypes.string,
    warehouseCode: PropTypes.string,
    packageData: PropTypes.object,
    numberStamp: PropTypes.number,
    classPrint: PropTypes.string,
};

export default withTranslation()(PrintStampPackage);