export const TYPE_PRINT = {
    PACKAGE: 'package',
    BAG: 'bag',
};

export const PRINT_PACKAGE_STAMP = "PRINT_PACKAGE_STAMP";
export const CLEAR_PRINT_STATE = "CLEAR_PRINT_STATE";