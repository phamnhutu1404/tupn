import {connect} from 'react-redux';
import PrintStamp from './components/PrintStamp';
import lodash from 'lodash';

const mapStateToProps = (state) => {
    return {
        dataPrint: lodash.get(state, 'common.dataPrint', {})
    };
};

export default connect(
    mapStateToProps
)(PrintStamp)