import {notification} from 'antd';
import lodash from 'lodash';
import {dispatch} from '../../system/store';
import {t} from '../../system/i18n';
import {HTTP_STATUS} from '../../system/api/constants';

/**
 * Xử lý api request
 */
export function processApiRequest(actionType, handler, request = {}) {
    let api = handler();

    api.then(response => dispatch({type: actionType.SUCCESS, payload: response.data, request}));

    api.catch(error => {
        let response = error.response;

        dispatch({
            type: actionType.FAILED,
            payload: response && response.status === HTTP_STATUS.BAD_REQUEST && response.data,
            request,
        });
    });
}

/**
 * Thông báo common error
 */
export function* onApiRequestError(action) {
    let status = lodash.get(action.payload, 'response.status');

    switch (status) {
        case HTTP_STATUS.BAD_REQUEST:
        case HTTP_STATUS.UNAUTHENTICATED:
            return;
        case HTTP_STATUS.UNAUTHORIZED:
            return yield notification.error({message: t('message.unauthorized')});
        default:
            return yield notification.error({message: t('message.server_error')});
    }
}

