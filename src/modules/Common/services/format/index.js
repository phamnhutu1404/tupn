import DateFormatter from './DateFormatter';
import CurrentFormatter from './CurrentFormatter';

export const dateFormatter = new DateFormatter();
export const currentFormatter = new CurrentFormatter();
