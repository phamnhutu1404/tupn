import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const setMenuActive = createAction(ACTION.SET_MENU_ACTIVE, menu => menu);
