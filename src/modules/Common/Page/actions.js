import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const updatePageTitle = createAction(ACTION.UPDATE_PAGE_TITLE);
