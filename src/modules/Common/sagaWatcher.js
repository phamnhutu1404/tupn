import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import {API} from '../../system/api/constants';

export default function*() {
    yield takeLatest(API.ERROR, saga.onApiRequestError);
}
