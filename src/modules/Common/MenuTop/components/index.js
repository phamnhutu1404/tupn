import React, {Component} from 'react';
import lodash from 'lodash';
import { Layout, Menu} from 'antd';
import PropTypes from 'prop-types';
import {url,router,Link} from './../../../../system/routing/index';

let MenuConfig = [
    // {
    //     'home':
    //         <Menu.Item key="/">
    //             Trang chủ
    //         </Menu.Item>,
    // },
    // {
    //     'intro':
    //         <Menu.Item key="intro">
    //             <Link to='intro'>Giới thiệu</Link>
    //         </Menu.Item>,
    // },
    // {
    //     'products':
    //         <Menu.Item key="products">
    //             <Link to='products'>Sản phẩm</Link>
    //         </Menu.Item>,
    // },
    // {
    //     'contact':
    //         <Menu.Item key="contact">
    //             <Link to='contact'>Liên hệ</Link>
    //         </Menu.Item>,
    // },
    // {
    //     'Quy định':
    //         <Menu.Item key="Quy định">
    //             Quy định
    //         </Menu.Item>,
    // },
    // {
    //     'Giỏ hàng':
    //         <Menu.Item key="sddd1">
    //             Giỏ hàng
    //         </Menu.Item>,
    // },
    // {
    //     'Giỏ hàng':
    //         <Menu.Item key="sddd4">
    //             Giày Boots
    //         </Menu.Item>,
    // },{
    //     'Giỏ hàng':
    //         <Menu.Item key="sddd2">
    //             Giày thể thao
    //         </Menu.Item>,
    // },,{
    //     'Giỏ hàng':
    //         <Menu.Item key="sddd3">
    //             Túi Xách
    //         </Menu.Item>,
    // },
];



class SiderX extends Component {
    /**
     * handle click
     * @param e
     */

    componentDidMount() {
        let params = lodash.get(router, 'location.pathname', '/');
        params = params.slice(1, params.length);
        this.props.setMenuActive(params || '/');
    }

    handleClick = (e) => {

        router.redirect(e.key);
        this.props.setMenuActive(e.key);
    };

    render() {
        let {menuActive} = this.props;
        
        return (
            <Menu theme="dark"
                  mode="horizontal" onClick={this.handleClick.bind(this)} style={{ lineHeight: '64px' }} defaultSelectedKeys={[menuActive]} selectedKeys={[menuActive]}
                  >
                {
                    MenuConfig.map((menu) => {
                        return menu[Object.keys(menu)[0]];

                    })
                }
            </Menu>
        )
    }
}

SiderX.defaultProps = {
    menuActive: '',
};

SiderX.propTypes = {
    menuActive: PropTypes.string,
};

export default SiderX