import {connect} from 'react-redux';
import SiderX from './components/';
import lodash from 'lodash';
import {setMenuActive} from './../actions';

const mapStateToProps = (state, props) => ({
    menuActive: lodash.get(state, 'common.menuActive', ''),
});

const mapDispatchToProps = (dispatch, props) => {
    return {
        setMenuActive: (params) => dispatch(setMenuActive(params)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SiderX);
