import React from 'react';
import PropTypes from 'prop-types';
import {withTranslation} from 'react-i18next';
import {Pagination, Select, Spin} from 'antd';

const Option = Select.Option;

class CustomPagination extends React.Component {
    handleSelectChange(value, param) {
        let {filter} = this.props;
        value = value || null;
        this.props.onChange({...filter, ...{page: 1, [param]: value}});
    }

    getPageSize() {
        return this.props.pageSize || 20;
    }

    render() {
        let pageSize = this.getPageSize();
        
        if (this.props.total <= pageSize) {
            return null;
        }

        return (
            <div className="a-pagination">
                {this.props.loading ? <Spin/> : null}
                <Pagination
                    current={this.props.page}
                    pageSize={pageSize}
                    total={this.props.total}
                    hideOnSinglePage={true}
                    className="a-pagination--detail"
                    onChange={this.props.onChange}
                />
                {this.props.showSizeChanger ? this.renderSizeChanger() : null}
            </div>
        )
    }

    renderSizeChanger() {
        return (
            <div className="a-pagination--select-number-item">
                <span className="text-show">{this.props.t('common:pagination.show_view')}</span>
                <Select
                    value={this.getPageSize()}
                    className="a-select--select-number-show"
                    onSelect={pageSize => this.props.onChange(this.props.page, pageSize)}
                >
                    {this.props.pageSizeOptions.map(size => <Option key={size} value={size}>{size}</Option>)}
                </Select>
            </div>
        );
    }
}

CustomPagination.defaultProps = {
    page: 1,
    pageSize: 20,
    total: 0,
    showSizeChanger: true,
    pageSizeOptions: [20, 30, 40, 50],
    loading: false,
    onChange: (page, pageSize) => {},
};

CustomPagination.propTypes = {
    page: PropTypes.number,
    pageSize: PropTypes.number,
    total: PropTypes.number,
    showSizeChanger: PropTypes.bool,
    pageSizeOptions: PropTypes.array,
    loading: PropTypes.bool,
    onChange: PropTypes.func,
};

export default withTranslation()(CustomPagination);
