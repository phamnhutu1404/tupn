import React, {PureComponent} from 'react';
import {Layout} from 'antd';
import PageTitle from '../Page/PageTitleContainer';
import UserPanel from '../../Auth/UserPanel/UserPanelContainer';

const {Header} = Layout;

class HeaderX extends PureComponent {
    render() {
        return (
            <Header className="a-header">
                <div className="a-header__left">
                    <PageTitle/>
                </div>
                <ul className="a-header__right">
                    <li className="info__user">
                        <UserPanel/>
                    </li>
                </ul>
            </Header>
        );
    }
}

export default HeaderX;
