import React from 'react';
import PropTypes from 'prop-types';
import lodash from 'lodash';
import {withTranslation} from 'react-i18next';
import {translateValidationErrors} from '../../Common/services/helps';

const FormError = ({attribute, errors, showAll, t}) => {
    if (!errors || !lodash.keys(errors).length) {
        return null;
    }

    let rules = lodash.keys(errors);

    if (!showAll) {
        rules = rules.slice(0, 1);
    }

    let messages = translateValidationErrors(attribute, lodash.pick(errors, rules));

    return (
        <ul className="list-unstyled invalid-feedback d-block mb-0">
            {rules.map(rule => <li key={rule}>{messages[rule]}</li>)}
        </ul>
    );
};

FormError.defaultProps = {
    showAll: false,
};

FormError.propTypes = {
    attribute: PropTypes.string,
    errors: PropTypes.object,
    showAll: PropTypes.bool,
};

export default withTranslation()(FormError);