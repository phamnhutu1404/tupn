import React from 'react';
import Background from '../../../resources/images/403.png';

export default () => (
    <div className="a-page--403"  style={{backgroundImage: `url(${Background})`,backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',}}>
        <h1 className="company-name">Xlogistic</h1>
        <div className="a-page--403__detail">
            <div className="title">
                <h3> Không có quyền truy cập</h3>
                <h1><span>4</span><span className="special">0</span><span>3</span></h1>
            </div>
            <h2>Xin lỗi, bạn không có quyền truy cập vào trang này! </h2>
            <button className="button-back">Trở về màn hình chính</button>
        </div>
    </div>
);