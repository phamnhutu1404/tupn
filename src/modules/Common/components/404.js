/* eslint-disable */
import React from 'react';
import Background from '../../../resources/images/404.png';
import {url,router} from './../../../system/routing/index';
import Link from "./../../../system/routing/Link";

export default () => (
    <div className="a-page--404" style={{backgroundImage: `url(${Background})`,backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',}}>
        <h1 className="company-name">NotFound</h1>
        <div className="a-page--404__detail">
            <div className="title">
                <h3>Không tìm thấy trang</h3>
                <h1><span>4</span><span>0</span><span>4</span></h1>
            </div>
            <h2>Xin lỗi, trang bạn truy cập hiện không tồn tại </h2>
            <Link to="home" className="button-back">Trở về màn hình chính</Link>
        </div>
    </div>
);