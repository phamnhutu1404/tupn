import React from 'react';
import PropTypes from 'prop-types';
import lodash from 'lodash';
import {Input} from 'antd';

class InputNumber extends React.PureComponent {
    constructor(props) {
        super(props);
        this.element = null;
    }

    focus() {
        if (this.element) {
            this.element.focus();
        }
    }

    /**
     * Xử lý khi input changed
     *
     * @param e
     */
    onChange(e) {
        let value = this.normalizeNumberInput(e.target.value || '');

        if (value === '' || this.makeRegExp().test(value)) {
            this.props.onChange(value);
        }
    }

    /**
     * Chuẩn hóa lại input theo format của number
     *
     * @param {string} value
     * @return {string}
     */
    normalizeNumberInput(value) {
        if (!value) {
            return value;
        }

        let {min} = this.props;

        // Nếu không cho nhập số âm thì loại bỏ ký tự '-'
        if (lodash.isNumber(min) && min >= 0) {
            value = value.replace(/-+/, '');
        }

        return value.replace(/[^0-9.]+/, '') // Loại bỏ các ký tự không hợp lệ
            .replace(/^(-?)\./, '$10.') // '.' => '0.'
            .replace(/^(-?)0+/, '$10') // 000 => 0
            .replace(/^(-?)0([1-9]+)/, '$1$2'); // 012 => 12
    }

    /**
     * Tạo regex để kiểm tra format của input
     *
     * @return {RegExp}
     */
    makeRegExp() {
        let {min, precision} = this.props;

        let pattern = (!lodash.isNumber(min) || min < 0) ? '-?[0-9]*' : '[0-9]+';

        if (precision !== 0) {
            let decimalRepeat = precision ? `{0,${precision}}` : '*';
            pattern += '(.[0-9]' + decimalRepeat + ')?';
        }

        return new RegExp('^' + pattern + '$');
    }

    /**
     * Xử lý number theo props khi blur input
     */
    onBlur() {
        const {value, onBlur, onChange} = this.props;
        const processedValue = this.processNumber(value);

        if (processedValue !== parseFloat(value)) {
            onChange(processedValue);
        }

        if (onBlur) {
            onBlur();
        }
    }

    /**
     * Xử lý number theo props
     *
     * @param {string} value
     * @return {number}
     */
    processNumber(value) {
        let {precision, min, max} = this.props;

        if (lodash.isString(value) && (value.charAt(value.length - 1) === '.' || value === '-')) {
            value = value.slice(0, -1);
        }

        value = parseFloat(value) || 0;

        if (lodash.isNumber(precision)) {
            value = lodash.round(value, precision);
        }

        if (lodash.isNumber(min)) {
            value = lodash.max([value, min]);
        }

        if (lodash.isNumber(max)) {
            value = lodash.min([value, max]);
        }

        return value;
    }

    render() {
        return <Input
            {...lodash.omit(this.props, ['precision', 'min', 'max'])}
            onChange={this.onChange.bind(this)}
            onBlur={this.onBlur.bind(this)}
            ref={element => this.element = element}
        />;
    }
}

InputNumber.defaultProps = {
    precision: null,
    min: null,
    max: null,
    onChange: (value) => {
    },
};

InputNumber.propTypes = {
    precision: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    onChange: PropTypes.func,
};

export default InputNumber;
