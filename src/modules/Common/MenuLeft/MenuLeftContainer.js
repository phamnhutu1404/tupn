import {connect} from 'react-redux';
import SiderX from './components/';
import lodash from 'lodash';

const mapStateToProps = (state) => ({
    menuActive: lodash.get(state, 'common.menuActive', '')
});

const mapDispatchToProps = (dispatch, props) => ({});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SiderX);
