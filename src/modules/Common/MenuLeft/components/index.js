import React, {Component} from 'react';
import { Layout, Menu} from 'antd';
import PropTypes from 'prop-types';
import {router} from '../../../../system/routing';
import {ReactComponent as Detail} from '../../../../resources/images/Menu/box.svg';


const {Sider} = Layout;

const MenuConfig = [

    {
        'html/barcode-scan':
            <Menu.Item key="packages/create">
                <div className="icon">
                    <Detail/>
                </div>
                <span className="title">Kiện Hàng</span>
            </Menu.Item>,
    },
    {
        'packagesList':
            <Menu.Item key="packages">
                <div className="icon">
                    <Detail/>
                </div>
                <span className="title">Danh sách kiện</span>
            </Menu.Item>,
    },
];

class SiderX extends Component {
    /**
     * handle click
     * @param e
     */
    handleClick = (e) => {
        router.redirect("/" + e.key);
    };

    render() {
        let {menuActive} = this.props;
        return (
            <Sider className="a-sider-main">
                <div className="a-logo">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0Hf2mgQMefYOfouTjyup7z__t6apm6B-lgaimKMh6jcxZe4R4uA"  alt=""/>
                    <span>G-COMPANY</span>
                </div>
                <Menu defaultSelectedKeys={[menuActive]} selectedKeys={[menuActive]} className="a-sider-main__menu"
                      onClick={this.handleClick}>
                    {
                        MenuConfig.map((menu) => {
                            return menu[Object.keys(menu)[0]];
                        })
                    }
                </Menu>
            </Sider>
        )
    }
}

SiderX.defaultProps = {
    menuActive: '',
};

SiderX.propTypes = {
    menuActive: PropTypes.string,
};

export default SiderX