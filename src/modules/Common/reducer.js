import {combineReducers} from 'redux';
import page from './Page/reducer';
import * as COMMON_ACTION from './constants';
import * as PRINT_ACTION from './PrintStamp/constants';

let menuActive = (state = '', action) => {
    let {type} = action;

    switch (type) {
        case COMMON_ACTION.SET_MENU_ACTIVE:
            return action.payload;
        default:
            return state;
    }
};

/**
 * state phuc vu phan in tem. State nay chua cac propeties sau: {packageData, warehouseCode, typePrint, numberStamp}
 * @param state
 * @param action
 * @returns {{}}
 */
let dataPrint = (state = {}, action) => {
    let {type} = action;
    switch (type) {
        case PRINT_ACTION.PRINT_PACKAGE_STAMP:
            return {
                packageData: action.payload.packageData,
                warehouseCode: action.payload.warehouseCode,
                typePrint: PRINT_ACTION.TYPE_PRINT.PACKAGE,
                numberStamp: action.payload.numberStamp,
            };
        case PRINT_ACTION.CLEAR_PRINT_STATE:
            return {};
        default:
            return state;
    }
};

export default combineReducers({
    page,
    menuActive,
    dataPrint,
});
