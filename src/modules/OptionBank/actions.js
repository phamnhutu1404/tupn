import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const getServices = createAction(ACTION.GET_SERVICES.REQUEST, data => data);
export const createRequest = createAction(ACTION.CUSTOMER_CREATE_REQUEST.REQUEST, data => data);
export const clearState = createAction(ACTION.CLEAR_STATE);
