import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import OptionService from './components/index'
import {getServices, createRequest} from './actions'
import {getUser} from './../Login/actions'

const mapStateToProps = (state, props) => {
    const {userData = []} = state.user
    const {loading, listServices = []} = state.bankServices
    return {
        loading,
        userData,
        listServices
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchServices: (params) => {
            dispatch(getServices(params));
        },
        getUser: (params) => {
            dispatch(getUser(params));
        },
        createRequest: (params) => {
            dispatch(createRequest(params));
        },
    };
};

let Container = connect(
    mapStateToProps,
    mapDispatchToProps
)(OptionService);

Container.defaultProps = {
    input: {},
    canCreate: false,
};

Container.propTypes = {
    input: PropTypes.object,
    canCreate: PropTypes.bool,
};

export default Container;

