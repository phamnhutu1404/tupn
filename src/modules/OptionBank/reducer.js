import {combineReducers} from 'redux';
import * as ACTION from './constants';

let loading = (state = false, action) => {
    switch (action.type) {
        case ACTION.GET_SERVICES.REQUEST:
            return true;

        case ACTION.GET_SERVICES.SUCCESS:
        case ACTION.GET_SERVICES.FAILED:
        case ACTION.CLEAR_STATE:
            return false;
        default:
            return state;
    }
};

let listServices = (state = [], action) => {
    switch (action.type) {
        case ACTION.GET_SERVICES.REQUEST:
            return [];
        case ACTION.GET_SERVICES.FAILED:
        case ACTION.CLEAR_STATE:
            return [];
        case ACTION.GET_SERVICES.SUCCESS:
            return action.payload
        default:
            return state;
    }
};

let createRequest = (state = {}, action) => {
    switch (action.type) {
        case ACTION.CUSTOMER_CREATE_REQUEST.REQUEST:
            return {};
        case ACTION.CUSTOMER_CREATE_REQUEST.FAILED:
        case ACTION.CLEAR_STATE:
            return {};
        case ACTION.CUSTOMER_CREATE_REQUEST.SUCCESS:
            return action.payload
        default:
            return state;
    }
};

export default combineReducers({
    loading,
    listServices,
    createRequest
});
