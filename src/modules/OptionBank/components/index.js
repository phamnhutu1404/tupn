import React from 'react'
import {Row, Col, Divider, Checkbox, Radio, Button, Form} from 'antd'
import {url,router} from './../../../system/routing/index'
import UserDropDown from './../../../components/UserDropDown/index'

const radioStyle = {
    display: 'block',
    height: '30px',
    lineHeight: '30px',
    margin: '5px'
};
class Services extends React.Component {

    constructor(props) {
        super(props);
        this.state = {category:1};
    }

    componentDidMount() {
        if( !Object.keys(this.props.userData || {}).length ){
            this.props.getUser();
        }
        this.props.fetchServices();
    }

    submitForm = (services) => {
        this.props.createRequest({clientIdentityNumber: 1, category: parseInt(services['serviceItem'] || "1"), "status": 2, username: this.props.userData['username']})
        //router.redirect(url.to('service.code'))
    }

    render() {
        const {userData = {}, listServices=[]} = this.props
        return (
            <div className={'home-content'}>
                <Row>
                    <Col xs={2} sm={4} md={6} lg={8} xl={10}>
                        <UserDropDown userData={userData}/>
                    </Col>
                    <Col xs={20} sm={16} md={12} lg={8} xl={4}>
                        <Divider className={'title-category'} orientation="left">
                            Choose our services
                        </Divider>
                        <Form onFinish={this.submitForm}>
                            <Row>
                                <Form.Item name="serviceItem">
                                    <Radio.Group>
                                        {listServices.map(item=>{
                                            return <Radio key={item.value} value={item.value} style={radioStyle}>{item.label}</Radio>
                                        })}
                                    </Radio.Group>
                                </Form.Item>
                            </Row>
                            &nbsp;
                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Request bank teller
                                </Button>
                            </Form.Item>
                        </Form>
                    </Col>
                    <Col xs={2} sm={4} md={6} lg={8} xl={10}/>
                </Row>
            </div>
        )
    }
}

export default Services
