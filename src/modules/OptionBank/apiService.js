import {api} from '../../system/api';
import {BANK_ROOM} from "./../Login/constants";
import {socket} from "./../../system/store/index";

export default {
    //getListProduct: (barcode) => api.get(`packages/barcode/${barcode}`, {singleRequest: true}),
    getServices: (params) => new Promise((resolve, reject) => {
        const data = [
                { name:'1',value:'1', label: 'Open card'},
                { name:'2',value:'2', label: 'Deposit'},
                { name:'3',value:'3', label: 'Withdraw'},
                { name:'4',value:'4', label: 'Lock account'}
        ]
        resolve({error: false, data});
    }),
    createRequestServices: (params) => api.post('ticket', {...params}, {singleRequest: true}),
    // createRequestServices: (params) => new Promise((resolve, reject) => {
    //
    //
    //
    //     let code_request = Math.floor(Math.random() * 101)
    //     socket.emit('joinBank', {...params, name: params.username, room: BANK_ROOM}, (error) => {
    //         if(error) {
    //             //alert(error);
    //         }
    //     });
    //     socket.emit('sendMessageBank', {...params, code_request: code_request}, () => {} );
    //     resolve({error: false, data: {...params, name: params.username, room: BANK_ROOM, code_request: code_request }});
    // })
}
