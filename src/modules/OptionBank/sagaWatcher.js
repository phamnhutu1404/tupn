import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.GET_SERVICES.REQUEST, saga.getServices);
    yield takeLatest(ACTION.CUSTOMER_CREATE_REQUEST.REQUEST, saga.createRequestService);
    yield takeLatest(ACTION.CUSTOMER_CREATE_REQUEST.SUCCESS, saga.createRequestServiceSuccess);
}
