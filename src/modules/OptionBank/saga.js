import {notification} from 'antd';
import {t} from './../../system/i18n';
import {processApiRequest} from './../Common/saga';
import * as ACTION from './constants';
import apiService from './apiService';
import {BANK_ROOM} from "./../Login/constants";
import {socket} from "./../../system/store/index";

export function* getServices(action) {
    yield processApiRequest(
        ACTION.GET_SERVICES,
        () => apiService.getServices(action.payload),
        action.payload
    );
}

export function* createRequestService(action) {
    yield processApiRequest(
        ACTION.CUSTOMER_CREATE_REQUEST,
        () => apiService.createRequestServices(action.payload),
        action.payload
    );
}

export function* createRequestServiceSuccess(action) {
    const {payload = {}} = action
        let code_request = action.payload['id']
        socket.emit('joinBank', {...payload, name: payload['client_identity_number'], room: BANK_ROOM}, (error) => {
            if(error) {
                //alert(error);
            }
        });
        socket.emit('sendMessageBank', {...payload, code_request: code_request}, () => {} );
    yield notification.success({message: t('package:create_same_barcode.message_create_package_success')});
}

export function* onCreatePackageSameBarcodeFailed() {
    yield notification.error({message: t('package:create_same_barcode.message_create_package_failed'), duration: 0});
}
