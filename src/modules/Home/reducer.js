import {combineReducers} from 'redux';
import * as ACTION from './constants';

let loading = (state = false, action) => {
    switch (action.type) {
        case ACTION.GET_PRODUCT_HOME.REQUEST:
            return true;

        case ACTION.GET_PRODUCT_HOME.SUCCESS:
        case ACTION.GET_PRODUCT_HOME.FAILED:
        case ACTION.CLEAR_STATE:
            return false;
        default:
            return state;
    }
};

let listProduct = (state = [], action) => {
    switch (action.type) {
        case ACTION.GET_PRODUCT_HOME.REQUEST:
            return [];
        case ACTION.GET_PRODUCT_HOME.FAILED:
        case ACTION.CLEAR_STATE:
            return [];
        case ACTION.GET_PRODUCT_HOME.SUCCESS:
            return action.payload
        default:
            return state;
    }
};

export default combineReducers({
    loading,
    listProduct
});
