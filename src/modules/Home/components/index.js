import React from 'react';
import ReactDOM from 'react-dom';
import { PDFExport, savePDF } from '@progress/kendo-react-pdf';
import { Grid, GridColumn as Column } from '@progress/kendo-react-grid';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { Row, Col, Divider } from 'antd';
import {url,router,Link} from './../../../system/routing/index';
import ListItem from './ListItem';
import BlockItem from './Block';
import queryString from "query-string";

// const data = [{
//                 category:{id:1, title:'Giay cong so 1'},
//                 listProduct: [ {id:1, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:2, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:3, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:4, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:5, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:6, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:7, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:8, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:9, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:10, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:11, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:12, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:13, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:14, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:15, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//                     {id:16, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"}]
//     },
//     {
//         category:{id:1, title:'Giay cong so 2'},
//         listProduct: [ {id:1, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:2, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:3, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:4, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:5, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:6, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:7, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:8, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:9, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:10, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:11, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:12, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:13, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:14, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:15, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:16, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"}]
//     },
//     {
//         category:{id:1, title:'Giay cong so 2'},
//         listProduct: [ {id:1, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:2, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:3, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:4, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:5, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:6, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:7, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:8, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:9, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:10, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:11, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:12, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:13, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:14, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:15, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
//             {id:16, src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"}]
//     },
//     ]
class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    // componentDidMount() {
    //     const {location, match} = this.props;
    //     let filters = {...queryString.parse(location.search), ...match.params};
    //     this.props.fetchProduct(filters);
    // }

    componentDidMount() {
        if( !Object.keys(this.props.userData || {}).length ){
            this.props.getUser();
        }
    }

    render() {
        const {listProduct = []} = this.props
        return (
            <div className={'home-content'}>
                {listProduct.map((dataItem, index)=>
                    <BlockItem category={dataItem.category} data={dataItem.listProduct} key={index}/>
                )}
            </div>
        );
    }
}

export default Home;
