import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Home from './components/index'
import {getListProduct} from './actions'
import {getUser} from "../../modules/Login/actions";

const mapStateToProps = (state, props) => {
    const {loading, listProduct = []} = state.home
    return {
        loading,
        listProduct
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchProduct: (filter) => {
            dispatch(getListProduct(filter));
        },
        getUser: (params) => {
            dispatch(getUser(params));
        },
    };
};

let Container = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

Container.defaultProps = {
    input: {},
    canCreate: false,
};

Container.propTypes = {
    input: PropTypes.object,
    canCreate: PropTypes.bool,
};

export default Container;

