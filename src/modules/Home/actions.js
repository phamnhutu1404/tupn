import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const getListProduct = createAction(ACTION.GET_PRODUCT_HOME.REQUEST, data => data);
export const clearState = createAction(ACTION.CLEAR_STATE);
