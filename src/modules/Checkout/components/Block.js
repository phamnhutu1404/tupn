import React from 'react'
import { Row, Divider } from 'antd'
import ListItem from './ListItem'

class Block extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        const {title, data=[]} = this.props
        return (
            <React.Fragment>
                <Divider className={'title-category'} orientation="left" >
                    {title}
                </Divider>
                <Row >
                    {data.map(item=>{return <ListItem item={item} key={item.id}/>})}
                </Row>
            </React.Fragment>
        );
    }
}

export default Block
