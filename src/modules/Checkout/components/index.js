import React from 'react'
import {Row, Form, Alert ,Table, Radio, Button, Empty, InputNumber} from 'antd'
import {url,router,Link} from './../../../system/routing/index'
import Customer from './Customer'
const ButtonGroup = Button.Group

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectionType: [],
            formData: {}
        };
    }

    componentDidMount() {
        this.props.getListCart();
    }

    componentWillUnmount() {
        this.props.clearState();
    }

    onSubmit = (formData) => {
        const {cartList, createOrder} = this.props
        formData = {...formData, order: cartList}
        createOrder(formData)
        console.log('Received values of form: ', formData)
    }

    render() {
        const {selectionType} = this.state
        const {cartList, loading, customer} = this.props
        const columns = [
            {
                title: 'San pham',
                dataIndex: 'info_product',
                render: (text, record) =>
                    <div className={'info-product'}>
                        <Link to={'home'}>
                            <img width={80} style={{float:'left'}} src={record.src} alt={record.title}/>&nbsp;
                            <span className={'info-product-title'}>{record.title}</span>
                        </Link>
                    </div>,
            },
            {
                title: 'Don gia',
                dataIndex: 'price',
                render: (text, record) =>
                    <div className={'product-price'}>
                        <span>650.000</span>
                        <span>₫</span>
                    </div>
            },
            {
                title: 'So luong',
                dataIndex: 'quantity',
                render: (text, record) =>
                    <div className="ant-btn-group">
                        {record.quantity}
                    </div>,
            },
            {
                title: 'Thành tiền',
                dataIndex: 'amount',
                render: (text, record) =>
                    <div className={'product-price'}>
                        <span>{record.amount}</span>
                        <span>₫</span>
                    </div>
            }
        ];

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 },
            },
        };



        return (
            <div className={'cart-content'}>
                <Alert
                    message="Success Tips"
                    description="Detailed description and advice about successful copywriting."
                    type="success"
                    showIcon
                />
                {cartList.length? <React.Fragment>
                    <Form
                        {...formItemLayout}
                        onFinish={this.onSubmit.bind(this)}
                    >
                        <Customer />
                        {/*<Table*/}
                        {/*    className={'cart-content__item-product'}*/}
                        {/*    columns={columns}*/}
                        {/*    dataSource={cartList}*/}
                        {/*    pagination={false}*/}
                        {/*/>*/}
                        <div style={{textAlign:'right'}}>
                            <Button size='large' htmlType="submit" type="danger" loading={loading}>Đặt hàng</Button>
                        </div>
                    </Form>
                </React.Fragment>
                    :
                    <React.Fragment>
                    <Empty>
                        <Link to={'home'}><Button size='large'>Mua ngay</Button></Link>
                    </Empty>
                </React.Fragment>}
            </div>
        );
    }
}

export default Cart;
