import React from 'react';
import {Row, Divider, Form, Select, Radio, Button, Input, Col} from 'antd';
const { Option } = Select;
class Customer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    onChange = (event)=>{
        console.log(event.target.value,'onChange',event.target.name)
    }

    render() {
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 5 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 12 },
            },
        };
        const {title, data=[]} = this.props

        const prefixSelector = (
            <Form.Item name="prefix" nostyle={false}>
                <Select style={{ width: 70 }}>
                    <Option value="84">+84</Option>
                </Select>
            </Form.Item>
        );

        return (
            <React.Fragment>
                <Divider className={'title-category'} orientation="left" >
                    {'Thông tin khách hàng'}
                </Divider>
                <Row >
                    <Col span={8} className={'home-content__item-product'}>
                        <Form.Item name={['user', 'username']} label="Họ và Tên" rules={[{ required: true, message: 'Please input your username!' }, ]}>
                            <Input autoComplete={'off'} value='tupn' />
                        </Form.Item>
                        <Form.Item name={['user', 'email']} label="Địa chỉ email" rules={[
                            { required: true, message: 'Please input your email!' },
                            { pattern: /^\S+@\S+\.\S+$/gim, message: 'Không đúng định dạng email'}]}>
                            <Input autoComplete={'off'} value='tupn@neotiq.net'/>
                        </Form.Item>
                    </Col>

                    <Col span={8} className={'home-content__item-product'}>
                        <Form.Item
                            name={['user', 'phone']}
                            label="Số điện thoại"
                            rules={[
                                { required: true, message: 'Please input your phone number!' },
                                { pattern: /^[0-9]+$/, message: 'Không phải số điện thoại vui lòng nhập lại'}
                                ]
                            }
                        >
                            <Input autoComplete={'off'} value='356091913' addonBefore={ <Input.Group compact>
                                <Select defaultValue="+84">
                                    <Option value="+84">+84</Option>
                                </Select>
                            </Input.Group>} style={{ width: '100%' }} />
                        </Form.Item>

                        <Form.Item name={['user', 'address']} label="Địa chỉ nhận" rules={[{ required: true , message: 'Input something!'}]}>
                            <Input.TextArea autoComplete={'off'} value='Ha Noi' />
                        </Form.Item>
                    </Col>
                    <Col span={8} className={'home-content__item-product'}>
                        <Form.Item name={['user', 'note']} label="Ghi chú">
                            <Input.TextArea autoComplete={'off'} />
                        </Form.Item>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default Customer;
