import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const getListCart = createAction(ACTION.GET_CART.REQUEST, data => data);
export const createOrder = createAction(ACTION.CREATE_ORDER.REQUEST, data => data);
export const clearState = createAction(ACTION.CLEAR_STATE);
