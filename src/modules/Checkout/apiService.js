import {api} from '../../system/api';

export default {
    //getListCart: (params) => api.get(`packages/barcode`,{singleRequest: true}),
    getListCart: (params) => new Promise((resolve, reject) => {
        // let data = [
        //     {
        //         key: '1',
        //         name: 'Mike',
        //         age: 32,
        //         address: '10 Downing Street',
        //     },
        //     {
        //         key: '2',
        //         name: 'John',
        //         age: 42,
        //         address: '10 Downing Street',
        //     },
        // ];
        let data = [
            {id:1,  key: '1', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
            {id:2,  key: '2', quantity: 1, price:1000, amount:'200.000', src:'https://cf.shopee.vn/file/6ebf14f5a4ef6723a22e7b09e83d9fa6', title:"Giày Cao Gót Nữ Đế Trong Suốt Siêu Xinh"},
        ]
        resolve({error: false, data});
    }),
    //createOrder: (params) => api.post(`packages/${params.packageCode}/update-weight`, {...params}, {singleRequest: true}),
    createOrder: (params) => new Promise((resolve, reject) => {
        console.log(params,'params')
        resolve({error: false, data: params});
    }),
    clearCreateOrder: (params) => new Promise((resolve, reject) => {
        console.log(params,'params')
        resolve({error: false, data: params});
    })
};
