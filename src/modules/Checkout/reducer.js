import {combineReducers} from 'redux';
import * as ACTION from './constants';
import * as ACTIONCART from './../Cart/constants';

let loading = (state = false, action) => {
    switch (action.type) {
        case ACTIONCART.GET_CART.REQUEST:
        case ACTION.CREATE_ORDER.REQUEST:
            return true;

        case ACTIONCART.GET_CART.SUCCESS:
        case ACTIONCART.GET_CART.FAILED:
        case ACTION.CREATE_ORDER.SUCCESS:
        case ACTION.CREATE_ORDER.FAILED:
        case ACTIONCART.CLEAR_STATE:
        case ACTION.CLEAR_STATE:
            return false;

        default:
            return state;
    }
};

let cartList = (state = [], action) => {
    switch (action.type) {
        case ACTION.GET_CART.REQUEST:
            return []
        case ACTION.GET_CART.FAILED:
        case ACTION.CLEAR_STATE:
            return []
        case ACTION.GET_CART.SUCCESS:
            return action.payload
        default:
            return state
    }
}

let customer = (state = {}, action) => {
    switch (action.type) {
        case ACTION.CREATE_ORDER.REQUEST:
            return {}
        case ACTION.CREATE_ORDER.FAILED:
        case ACTION.CLEAR_STATE:
            return {}
        case ACTION.CREATE_ORDER.SUCCESS:
            return action.payload
        default:
            return state
    }
}

export default combineReducers({
    loading,
    cartList,
    customer,
});
