import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getListCart, createOrder, clearState} from './actions';
import CartContainer from './components/index';

const mapStateToProps = (state, props) => {
    const {loading, cartList, customer = {}} = state.checkout
    return {
        loading,
        cartList,
        customer,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getListCart: ()=>{
            dispatch(getListCart())
        },
        createOrder: (filter)=>{
            dispatch(createOrder(filter))
        },
        clearState : (filter) =>{
            dispatch(clearState(filter))
        }
    };
};

let Container = connect(
    mapStateToProps,
    mapDispatchToProps
)(CartContainer);

Container.defaultProps = {
    input: {},
    canCreate: false,
};

Container.propTypes = {
    input: PropTypes.object,
    canCreate: PropTypes.bool,
};

export default Container;

