import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.GET_CART.REQUEST, saga.getListCart)
    yield takeLatest(ACTION.CREATE_ORDER.REQUEST, saga.createOrder)
    yield takeLatest(ACTION.CLEAR_STATE, ()=>{})
}
