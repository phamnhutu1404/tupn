import {takeLatest} from 'redux-saga/effects';
import * as saga from './saga';
import * as ACTION from './constants';

export default function*() {
    yield takeLatest(ACTION.GET_CONTACT.REQUEST, saga.createPackageSameBarcode);
    yield takeLatest(ACTION.GET_CONTACT.SUCCESS, saga.onCreatePackageSameBarcodeSuccess);
    yield takeLatest(ACTION.GET_CONTACT.FAILED, saga.onCreatePackageSameBarcodeFailed);
}
