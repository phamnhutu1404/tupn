import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as actions from './actions';
import Home from './components/index';

const mapStateToProps = (state, props) => {
    return {
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
    };
};

let Container = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

Container.defaultProps = {
    input: {},
    canCreate: false,
};

Container.propTypes = {
    input: PropTypes.object,
    canCreate: PropTypes.bool,
};

export default Container;

