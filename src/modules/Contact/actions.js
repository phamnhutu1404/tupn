import {createAction} from 'redux-actions';
import * as ACTION from './constants';

export const createPackageSameBarcode = createAction(ACTION.GET_CONTACT.REQUEST, data => data);
export const clearState = createAction(ACTION.CLEAR_STATE);
