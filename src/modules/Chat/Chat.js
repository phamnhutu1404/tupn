import React, { useState, useEffect } from "react"
import queryString from 'query-string'
import Messages from './Messages'
import InfoBar from './InfoBar'
import Input from './Input'
import {connect} from "react-redux";
import {socket} from "./../../system/store/index";

const Chat = (props) => {
    const {authentication} = props
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [users, setUsers] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const [usersRecover, setUsersRecover] = useState([]);
    const [messageRecoverPassWord, setMessageRecoverPassWord] = useState([]);


    useEffect(() => {
        if( authentication && messageRecoverPassWord.filter(item=>item.userId === authentication.user.id && item.hasJoin === true).length ){
        }
    }, [messageRecoverPassWord]);

    useEffect(() => {
        const { name, room } = queryString.parse(props.location.search);
        setRoom(room);
        setName(name)
        socket.emit('join', { name, room }, (error) => {
            if(error) {
                alert(error);
            }
        });

    }, []);

    useEffect(() => {
        socket.on('message', message => {
            setMessages(messages => [ ...messages, message ]);
        });

        socket.on("roomData", ({ users }) => {
            setUsers(users);
        });

    }, []);

    const sendMessage = (event) => {
        event.preventDefault();
        if(message) {
            console.log(message,'message')
            socket.emit('sendMessage', message, () => setMessage(''));
        }
    }

    return (
        <div className="outerContainer">
            <div className="container">
                <InfoBar room={room} />
                <Messages messages={messages} name={name} />
                <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
            </div>
            {/*<TextContainer users={users} />*/}
            <p className={'text-center'}>
                {usersRecover.map(item=>item.userId)}
            </p>
            <p className={'text-center'}>
                {/*{console.log(messageRecoverPassWord,'messageRecoverPassWord', authentication.user.id)}*/}
                {/*{*/}
                {/*    if (messageRecoverPassWord.filter(item=>item.userId === authentication.user.id && item.hasJoin === true).length)*/}
                {/*    {*/}
                {/*    */}
                {/*    }*/}
                {/*}*/}
                {console.log( messageRecoverPassWord.filter(item=>item.userId === authentication.user.id && item.hasJoin === true).length,'sssssss' )}
                {messageRecoverPassWord.map(item=>item.text)}
            </p>
            <p className={'text-center'}>
                {/*{console.log(usersRecover,'usersRecover usersRecover')}*/}
                {usersRecover.map(item=>item.text)}
            </p>
        </div>
    );
}

const mapStateToProps = (state, props) => {
    const {authentication} = state;
    return {authentication}
};
const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat)