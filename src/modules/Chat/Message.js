import React from 'react';

const Message = ({ message: { text, user }, name }) => {
    let isSentByCurrentUser = false;

    const trimmedName = name.trim().toLowerCase();

    if(user === trimmedName) {
        isSentByCurrentUser = true;
    }

    return (
        isSentByCurrentUser
            ? (
                <div className="messageContainer justifyEnd">
                    <div className="messageBox backgroundBlue">
                        <p className="messageText colorWhite"><b>{trimmedName}</b>: {text}</p>
                    </div>
                </div>
            )
            : (
                <div className="messageContainer justifyStart">
                    <div className="messageBox backgroundLight">
                        <p className="messageText colorDark"><b>{user}</b>: {text}</p>
                    </div>
                </div>
            )
    );
}

export default Message;