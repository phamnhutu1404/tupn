import React from 'react';
import {Helmet} from "react-helmet";
import logo from './../logo.svg';
import 'antd/dist/antd.css';
import './../App.css';

import MenuTop from "../modules/Common/MenuTop/MenuTopContainer"
import MenuLeft from './MenuLeft';

import {
    Layout, BackTop, Carousel, Row, Col
} from 'antd';
import {Link} from "./../system/routing";

const {
    Header, Content, Footer,
} = Layout

const REACT_VERSION = React.version

export default ({children}) => (
    <div className="App">
        <Helmet>
            <title>Ideas || Mysite</title>
            <meta name="keywords" content="HTML,CSS,JavaScript"/>
            <meta name="description" content="Ideas page using react helmet very easy to implement"/>
        </Helmet>
        <Layout>
            <MenuLeft/>
            <Layout>
                <Header className="header"   style={{ background: '#fff', padding: 0 }}>
                    {<MenuTop/>}
                </Header>
                <Content style={{ margin: '24px 16px 0' }}>
                    <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
                        <Content className="banner-home" style={{ padding: '0 24px' }}>
                            <Carousel autoplay>
                                <div><h3><img src={logo} className="App-logo" alt="logo"/>1</h3></div>
                                <div><h3><img src={logo} className="App-logo" alt="logo"/>2</h3></div>
                                <div><h3><img src={logo} className="App-logo" alt="logo"/>3</h3></div>
                                <div><h3><img src={logo} className="App-logo" alt="logo"/>4</h3></div>
                            </Carousel>
                            <Row gutter={16}>
                                <Col span={6} style={{height:'10px', width:'100%'}}><p>&nbsp;</p></Col>
                            </Row>
                            {children}
                        </Content>
                    </div>
                </Content>
                <Content style={{ margin: '24px 16px 0', background:'#ffeb0d', color:'#333', padding:'15px' }}>
                    <Row>
                        <Col xs={24} sm={12} md={8} lg={8} xl={8} xxl={8}>
                            <span><b>Cơ sở 1</b></span><br/>

                            <span>VP Giao Dịch 28, tầng 28, Tòa CT10C Khu Đô Thị Đại Thanh- Thanh Trì- Hà Nội</span><br/>

                            <span>Hotline: (+84) 0961 680 922</span><br/>

                            <span>Thời gian làm việc: Từ 08h00 đến 12h00 & từ 13h30 đến 17h30 (Từ T2-T7) & CN từ 8h00-17h00</span>
                        </Col>
                        <Col xs={24} sm={12} md={8} lg={8} xl={8} xxl={8}>
                            <span><b>Cơ sở 2</b></span><br/>

                            <span>Số 155 ngách 394/16 Đường Mỹ Đình- Quận Nam Từ Liêm- Hà Nội</span><br/>

                            <span>Hotline: (+84) 0373 134 272</span><br/>

                            <span>Từ 08h00 đến 12h00 & từ 13h30 đến 17h30 (Từ T2 – T7)</span>
                        </Col>
                        <Col xs={24} sm={12} md={8} lg={8} xl={8} xxl={8}>
                            <ul>
                                <li>
                                    <Link to={"category.product"} params={{id: 1}}>

                                        <span className="nav-text">Gioi thieu</span>
                                    </Link>
                                </li>
                                <li>
                                    <Link to={"category.product"} params={{id: 1}}>

                                        <span className="nav-text">Gioi thieu</span>
                                    </Link>
                                </li>
                                <li>
                                    <Link to={"category.product"} params={{id: 1}}>

                                        <span className="nav-text">Gioi thieu</span>
                                    </Link>
                                </li>
                            </ul>
                        </Col>
                    </Row>
                </Content>
                <Footer style={{ textAlign: 'center' }}>
                    <BackTop />
                    <span>{REACT_VERSION} © 2020 Công Ty Tnhh Thời Trang Và Du Lịch Hồng Phú</span><br/>
                    <span>GPKD số 0109112600 do Sở Kế Hoạch và Đầu Tư HÀ NỘI cấp</span><br/>
                    <span>Email: dulichhongphu@gmail.com Điện thoại: 0961 680 922</span>
                </Footer>
            </Layout>
        </Layout>
    </div>
);
