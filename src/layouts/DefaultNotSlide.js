import React from 'react';
import {Helmet} from "react-helmet";
import 'antd/dist/antd.css';
import './../App.css';

import MenuTop from "../modules/Common/MenuTop/MenuTopContainer"
import MenuLeft from './MenuLeft';

import {
    Layout, BackTop, Row, Col,
} from 'antd'

const {
    Header, Content, Footer,
} = Layout


export default ({children}) => (
    <div className="App">
        <Helmet>
            <title>Ideas || Mysite</title>
            <meta name="keywords" content="HTML,CSS,JavaScript"/>
            <meta name="description" content="Ideas page using react helmet very easy to implement"/>
        </Helmet>
        <Layout>
            {/*<MenuLeft/>*/}
            <Layout>
                <Header className="header"   style={{ background: '#fff', padding: 0 }}>
                    {<MenuTop/>}
                </Header>
                <Content style={{ margin: '24px 16px 0' }}>
                    <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
                        <Content className="banner-home" style={{ padding: '0 24px' }}>
                            <Row gutter={16}>
                                <Col span={6} style={{height:'10px', width:'100%'}}><p>&nbsp;</p></Col>
                            </Row>
                            {children}
                        </Content>
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>
                    <BackTop />
                    <span>© 2020 Hanoï Office 5eme floor 54 ngo 9,Hoang Cau, Dong Da, Hanoi, Vietnam</span><br/>
                </Footer>
            </Layout>
        </Layout>
    </div>
);
