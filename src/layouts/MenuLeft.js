import React, {Component} from 'react';
import logo from './../logo.svg';
import PropTypes from 'prop-types';
import {router, Link} from './../system/routing/index';

import {
    Layout, Menu, Icon,
} from 'antd';

const {
    Header, Content, Footer, Sider,
} = Layout;

class SiderX extends Component {

    render() {
        return (
            <Sider
                breakpoint="lg"
                collapsedWidth="0"
                onBreakpoint={(broken) => {  }}
                onCollapse={(collapsed, type) => { }}
            >
                <div className="logo" style={{height:'65px', textAlign:'center',paddingTop:'15px',background: '#ffffff; !important'}}>
                    <img src={logo} className="App-logo" alt="logo"/></div>
                <Menu mode="inline" defaultSelectedKeys={['4']}>
                    <Menu.Item key="1">

                        <span className="nav-text">Trang chủ</span>
                    </Menu.Item>

                    <Menu.Item key="2">
                        <Link to={"category.product"} params={{id: 1}}>

                            <span className="nav-text">Gioi thieu</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="3">
                        <Link to={"category.product"} params={{id: 1}}>

                        <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="4">
                        <Link to={"category.product"} params={{id: 1}}>

                            <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="5">
                        <Link to={"category.product"} params={{id: 1}}>

                            <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="6">
                        <Link to={"category.product"} params={{id: 1}}>

                            <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="7">
                        <Link to={"category.product"} params={{id: 1}}>
                            <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="8">
                        <Link to={"category.product"} params={{id: 1}}>
                            <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="9">
                        <Link to={"category.product"} params={{id: 1}}>
                            <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="10">
                        <Link to={"category.product"} params={{id: 1}}>
                            <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="11">
                        <Link to={"category.product"} params={{id: 1}}>
                            <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="12">
                        <Link to={"category.product"} params={{id: 1}}>
                            <span className="nav-text">Lien he</span>
                        </Link>
                    </Menu.Item>
                </Menu>
            </Sider>
        )
    }
}

SiderX.defaultProps = {
    menuActive: '',
};

SiderX.propTypes = {
    menuActive: PropTypes.string,
};

export default SiderX