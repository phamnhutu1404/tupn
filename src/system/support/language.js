/**
 * Thay đổi language khi có thay đổi trên url ( mặc định là english ) <br />
 * @param router
 * @param i18n
 */
export function changeLanguageByRouter(router, i18n) {
    let search = router.history.location.search;

    i18n.changeLanguage('vi');

    if (localStorage.getItem("i18nextLng") === "en-US") {
        i18n.changeLanguage('en');
    }

    if (localStorage.getItem("i18nextLng") === "vi-VN") {
        i18n.changeLanguage('vi');
    }

    if (search.includes("lang=en")) {
        i18n.changeLanguage('en');
    }

    if (search.includes("lang=vi")) {
        i18n.changeLanguage('vi');
    }
}

/**
 * thêm vào param 1 key với value tương ứng
 * @param key
 * @param value
 */
export function insertParamIntoURL(key, value) {
    key = encodeURI(key);
    value = encodeURI(value);

    let kvp = document.location.search.substr(1).split('&');

    let i = kvp.length;
    let x;
    while (i--) {
        x = kvp[i].split('=');

        if (x[0] === key) {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }

    if (i < 0) {
        kvp[kvp.length] = [key, value].join('=');
    }

    //this will reload the page, it's likely better to store this until finished
    document.location.search = kvp.join('&');
}