import {getEnv} from '../system/support/helpers';

export default {
    /**
     * URL logout của OAuth server
     */
    logoutUrl: getEnv('REACT_APP_AUTH_LOGOUT_URL', 'https://id.gobizdev.com/confirm_logout'),

    /**
     * Thời hạn tồn tại của token (minutes)
     */
    tokenExpire: getEnv('REACT_APP_AUTH_TOKEN_EXPIRE', 0),
};