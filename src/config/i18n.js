import {getEnv} from '../system/support/helpers';

export default {
    /**
     * The current language
     */
    language: getEnv('REACT_APP_LANG', 'vi'),

    /**
     * The fallback language
     */
    fallbackLanguage: getEnv('REACT_APP_LANG_FALLBACK', 'vi'),
};