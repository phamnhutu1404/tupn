import {getEnv} from '../system/support/helpers';

export default {
    /**
     * The base api url
     */
    apiUrl: getEnv('REACT_APP_API_URL', 'http://localhost:3002/api/v1'),
};
