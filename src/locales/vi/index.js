import common from './common.json';
import validation from './validation.json';
import auth from './auth.json';
import layout from "./layout.json";
import packageLang from './package.json';
import location from './location.json';

export default {
    location,
    layout,
    common,
    validation,
    auth,
    package: packageLang,
};