import 'regenerator-runtime/runtime';
import {all, fork} from 'redux-saga/effects';
import CommonWatcher from './modules/Common/sagaWatcher';
import HomeWatcher from './modules/Home/sagaWatcher';
import IntroWatcher from './modules/Intro/sagaWatcher';
import ContactWatcher from './modules/Contact/sagaWatcher';
import ProductWatcher from './modules/Products/sagaWatcher';
import CartWatcher from './modules/Cart/sagaWatcher';
import CheckoutWatcher from './modules/Checkout/sagaWatcher';
import LoginWatcher from './modules/Login/sagaWatcher';
import optionBank from './modules/OptionBank/sagaWatcher';
import serviceCode from './modules/ServiceCode/sagaWatcher';
import manageBankTeller from './modules/ManageBankTeller/sagaWatcher';

export default function* start() {
    yield all([
        fork(CommonWatcher),
        fork(HomeWatcher),
        fork(IntroWatcher),
        fork(ContactWatcher),
        fork(ProductWatcher),
        fork(CartWatcher),
        fork(CheckoutWatcher),
        fork(LoginWatcher),
        fork(optionBank),
        fork(serviceCode),
        fork(manageBankTeller),
    ]);
}
