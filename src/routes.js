import DefaultLayout from './layouts/Default';
import DefaultNotSlide from './layouts/DefaultNotSlide';
import DefaultNull from './layouts/DefaultNull';

import homePage from './modules/Home/HomeContainer';
import introPage from './modules/Intro/IntroContainer';
import contactPage from './modules/Contact/ContactContainer';
import detailProduct from './modules/Products/Detail/DetailProductContainer';
import categoryProduct from './modules/Products/Category/CategoryProductContainer';
import cartProduct from './modules/Cart/CartContainer';
import checkoutModule from './modules/Checkout/CartContainer';
import loginForm from './modules/Login/LoginContainer';
import optionBank from './modules/OptionBank/OptionBankContainer';
import serviceCode from './modules/ServiceCode/OptionBankContainer';
import manageBankTeller from './modules/ManageBankTeller/ManageBankTellerContainer';

import Chat from './modules/Chat/Chat';
import Join from './modules/Chat/Join';

import Page404 from './modules/Common/components/404';

/*End*/
import commonMiddleware from './system/middlewares/commonMiddleware';
import i18n from './system/i18n/index';
import {router} from './system/routing/index';

export default {
    routes: [
        {
            group: {
                layout: DefaultLayout,
                middleware: [],
                routes: [
                   {
                        name: 'bank.login',
                        path: '/login',
                        exact: true,
                        component: loginForm,
                        layout: DefaultNull,
                    },{
                        name: 'bank.services',
                        path: '/services',
                        exact: true,
                        component: optionBank,
                        layout: DefaultNull,
                    },{
                        name: 'service.code',
                        path: '/service_code',
                        exact: true,
                        component: serviceCode,
                        layout: DefaultNull,
                    },{
                        name: 'manage.bank.teller',
                        path: '/manage_bank_teller',
                        exact: true,
                        component: manageBankTeller,
                        layout: DefaultNull,
                    },{
                        name: 'manage.Chat',
                        path: '/chat',
                        exact: true,
                        component: Chat,
                        layout: DefaultNull,
                    }
                    ,{
                        name: 'manage.join',
                        path: '/join',
                        exact: true,
                        component: Join,
                        layout: DefaultNull,
                    },
                ],
            },
        },
    ],
    defaultRoute: {
        component: Page404,
    },
    middleware: [
        (payload, next) => commonMiddleware(payload, next, router, i18n),
    ],
};
