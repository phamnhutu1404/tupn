// @ts-nocheck

/**
 * @returns {*|boolean}
 */
export function saveLocalStorage(key='CUSTOMER',params){
    localStorage.setItem(key, params)
}

/**
 * @returns {*|boolean}
 */
export function getLocalStorage(key='CUSTOMER'){
    let user = localStorage.getItem(key);
    user = user? JSON.parse(user) : {};
    return user
}

/**
 * @returns {*|boolean}
 */
export function removeLocalStorage(key='CUSTOMER'){
    localStorage.removeItem(key)
}