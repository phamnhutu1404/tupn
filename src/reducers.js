import {combineReducers} from 'redux';
import commonReducer from './modules/Common/reducer';
import homeReducer from './modules/Home/reducer';
import introReducer from './modules/Intro/reducer';
import contactReducer from './modules/Contact/reducer';
import productsReducer from './modules/Products/reducer';
import cartReducer from './modules/Cart/reducer';
import checkoutReducer from './modules/Checkout/reducer';
import loginReducer from './modules/Login/reducer';
import bankServices from './modules/OptionBank/reducer';
import serviceCode from './modules/ServiceCode/reducer';
import manageBankTeller from './modules/ManageBankTeller/reducer';

const rootReducer = combineReducers({
    'home': homeReducer,
    'intro': introReducer,
    'contact': contactReducer,
    'products': productsReducer,
    'common': commonReducer,
    'cart': cartReducer,
    'checkout': checkoutReducer,
    'user': loginReducer,
    'bankServices': bankServices,
    'serviceCode' : serviceCode,
    'manageBankTeller': manageBankTeller
});

export default rootReducer;
